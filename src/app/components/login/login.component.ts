import { Component } from '@angular/core';

@Component({
  selector: 'kypo-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {}
