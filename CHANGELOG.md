### 15.0.1 Sync sandbox agenda with sandbox service to accept paginated results.
* b609c3a3 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 60007e4c -- Bump sanbdox agenda
* e327c1eb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 61c98197 -- Add new ATD simulator with material styling
* 3b209326 -- Add fixed agendas
* f1c5768e -- Update all libraries to newest versions
* 0276655e -- Fix Docker node issue
* c908abbd -- Update packages to Angular 15
* cdc3928c -- Update mock DB
* bfc420cf -- Update to Angular 15
* f65a5e19 -- Updated versions of training agenda, api and model.
* 3adad9eb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 6fbfa717 -- Bump sandbox agenda library version.
* df7e818f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8fe29bdb -- Resolve "Update fixed agendas"
* 8bdf996e -- Update package versions
* b5070c37 -- Bump versions for bug fix release.
* 48692e28 -- Updated version and routing of cheating detection.
* 98562f12 -- Updated routing of components.
* 46711d11 -- Updated cheating detection routing modules.
* 92968cd2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* a97fd581 -- Develop
* e4be79b0 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9a8a0adc -- Merge develop into master
* 7e1bb505 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 415e8f2a -- Develop
* f3b97321 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* e040217f -- Develop
* 8ac83f44 -- Bump sandbox agenda and api
* afd3aea9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f83e2a8b -- Disable cheating detection
* 0f75542b -- Bump versions
* e15d64a5 -- Bump versions
* 33685b52 -- Resolve latest problems in sandbox agenda
* b0ee8a3b -- Added slider for sandbox allocation
* 475cc64a -- Added cheating detection. Disabled the routing as it is causing huge performance issues
* a9beb75a -- Bump sandbox agenda to enable sorting of the pool detail table
* 0f9182d0 -- Resolve problem regarding training run delete
* dbf12443 -- Fix build
* a55f43ef -- Bump sandbox uuid changes
* 8bd77ca3 -- Bump hurdling viz
* c47364f9 -- Bump walkthrough visualization to address problem with chrome responsibility
* cc8c5dee -- Fix integrity of package
* 87d71f17 -- Bump versions for stagining testing
* 9ebb79db -- Bump sandbox agenda - reflect changes in resource pages
* 63c9d005 -- Update to latest changes in training agenda and command visualization
* 05e8f948 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   c604188a -- Merge branch 'develop' into 'master'
|\  
| * 5e22e370 -- Address changes in sandbox agenda.
|/  
* 6e6ebb59 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   47aec793 -- Merge branch 'develop' into 'master'
|\  
| * f3566053 -- Develop
|/  
* 5ef6d630 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   5fe82f54 -- Merge branch 'develop' into 'master'
|\  
| * a271aa8a -- Develop
|/  
* e572094b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   61a36494 -- Merge branch 'develop' into 'master'
|\  
| * ebb054f5 -- Develop
|/  
* fb29c496 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   094e8462 -- Merge branch 'develop' into 'master'
|\  
| * d640d8a9 -- Merge develop into master
|/  
* 2f6a791a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a8f74328 -- Merge branch 'develop' into 'master'
|\  
| * 5b90d9e7 -- Develop
|/  
* 3af8daf2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   36caaa2d -- Merge branch '551-bump-patch-version-of-training-agenda' into 'master'
|\  
| * a4b7a473 -- Resolve "Bump patch version of training agenda"
|/  
* 862c05e6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   da9a99f8 -- Merge branch '550-bump-versions-of-agendas' into 'master'
|\  
| * 94cb4df8 -- Resolve "Bump versions of agendas"
|/  
* 10664b3e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   335edd22 -- Merge branch 'develop' into 'master'
|\  
| * 117cdf64 -- Develop
|/  
* 66a67a89 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   bb2c4520 -- Merge branch '549-add-routing-for-run-visualization-tabs' into 'master'
|\  
| * c78c19a3 -- Resolve "Add routing for run visualization tabs"
|/  
* 410199da -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0d85b374 -- Merge branch 'develop' into 'master'
|\  
| * af863b0c -- Develop
|/  
* f092bd09 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0ae646c2 -- Merge branch 'develop' into 'master'
|\  
| * 0ff7f79e -- Create release candidate tag
|/  
* 31a8ec8f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a28b2dcb -- Merge branch 'fix-failing-pipeline' into 'master'
|\  
| * 743238e3 -- Fix failing pipeline
|/  
* 9f32a818 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   191c1c5a -- Merge branch '546-fix-failing-docker-push-pipeline-stage' into 'master'
|\  
| * 9ef9dcab -- Resolve "Fix failing docker push pipeline stage"
|/  
* b144800b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   6798b6a5 -- Merge branch '545-update-to-angular-13' into 'master'
|\  
| * 0452dbf7 -- Resolve "Update to Angular 13"
|/  
*   e6921b11 -- Merge branch '544-remove-tour-guide' into 'master'
|\  
| * 64eba553 -- Remove Tour guide
|/  
* 82d9c597 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   534273f0 -- Merge branch '543-bump-version' into 'master'
|\  
| * 66c4805a -- Bump version
|/  
* 1dc638f4 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   dcb7eea7 -- Merge branch '541-bump-version-of-sandbox-agenda' into 'master'
|\  
| * 577b9964 -- Resolve "Bump version of sandbox agenda"
|/  
* 9a5b7e4b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   95a7ea80 -- Merge branch '540-release-testing-fix-found-issues' into 'master'
|\  
| * 23d5291f -- Latest release
|/  
* 63200336 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   50ef18d5 -- Merge branch '539-release-new-version' into 'master'
|\  
| * 7987f0e0 -- Resolve "Release new version"
|/  
* fcfa85cb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   d853702e -- Merge branch '538-latest-versions-update' into 'master'
|\  
| * 45b66042 -- Resolve "Latest versions update"
|/  
* 56660793 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   792cfe4f -- Merge branch '536-release-2021-12-0' into 'master'
|\  
| * b5948d67 -- Resolve "Release 2021.12.0"
|/  
*   470388a4 -- Merge branch '534-add-license-file' into 'master'
|\  
| * 0a8ddb6d -- Add license file
|/  
* 1e2e474b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   43180dec -- Merge branch '532-update-version-of-the-sandbox-agenda-and-training-agenda-due-to-guacamole-integration' into 'master'
|\  
| * c882bb6d -- Itegration of the Apache Guacamole - bump versions of the topology graph, training agenda and sandbox agenda.
|/  
* 076c437a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 15.0.1 Sync sandbox agenda with sandbox service to accept paginated results.
* 60007e4c -- Bump sanbdox agenda
* e327c1eb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 61c98197 -- Add new ATD simulator with material styling
* 3b209326 -- Add fixed agendas
* f1c5768e -- Update all libraries to newest versions
* 0276655e -- Fix Docker node issue
* c908abbd -- Update packages to Angular 15
* cdc3928c -- Update mock DB
* bfc420cf -- Update to Angular 15
* f65a5e19 -- Updated versions of training agenda, api and model.
* 3adad9eb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 6fbfa717 -- Bump sandbox agenda library version.
* df7e818f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8fe29bdb -- Resolve "Update fixed agendas"
* 8bdf996e -- Update package versions
* b5070c37 -- Bump versions for bug fix release.
* 48692e28 -- Updated version and routing of cheating detection.
* 98562f12 -- Updated routing of components.
* 46711d11 -- Updated cheating detection routing modules.
* 92968cd2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* a97fd581 -- Develop
* e4be79b0 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9a8a0adc -- Merge develop into master
* 7e1bb505 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 415e8f2a -- Develop
* f3b97321 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* e040217f -- Develop
* 8ac83f44 -- Bump sandbox agenda and api
* afd3aea9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f83e2a8b -- Disable cheating detection
* 0f75542b -- Bump versions
* e15d64a5 -- Bump versions
* 33685b52 -- Resolve latest problems in sandbox agenda
* b0ee8a3b -- Added slider for sandbox allocation
* 475cc64a -- Added cheating detection. Disabled the routing as it is causing huge performance issues
* a9beb75a -- Bump sandbox agenda to enable sorting of the pool detail table
* 0f9182d0 -- Resolve problem regarding training run delete
* dbf12443 -- Fix build
* a55f43ef -- Bump sandbox uuid changes
* 8bd77ca3 -- Bump hurdling viz
* c47364f9 -- Bump walkthrough visualization to address problem with chrome responsibility
* cc8c5dee -- Fix integrity of package
* 87d71f17 -- Bump versions for stagining testing
* 9ebb79db -- Bump sandbox agenda - reflect changes in resource pages
* 63c9d005 -- Update to latest changes in training agenda and command visualization
* 05e8f948 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   c604188a -- Merge branch 'develop' into 'master'
|\  
| * 5e22e370 -- Address changes in sandbox agenda.
|/  
* 6e6ebb59 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   47aec793 -- Merge branch 'develop' into 'master'
|\  
| * f3566053 -- Develop
|/  
* 5ef6d630 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   5fe82f54 -- Merge branch 'develop' into 'master'
|\  
| * a271aa8a -- Develop
|/  
* e572094b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   61a36494 -- Merge branch 'develop' into 'master'
|\  
| * ebb054f5 -- Develop
|/  
* fb29c496 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   094e8462 -- Merge branch 'develop' into 'master'
|\  
| * d640d8a9 -- Merge develop into master
|/  
* 2f6a791a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a8f74328 -- Merge branch 'develop' into 'master'
|\  
| * 5b90d9e7 -- Develop
|/  
* 3af8daf2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   36caaa2d -- Merge branch '551-bump-patch-version-of-training-agenda' into 'master'
|\  
| * a4b7a473 -- Resolve "Bump patch version of training agenda"
|/  
* 862c05e6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   da9a99f8 -- Merge branch '550-bump-versions-of-agendas' into 'master'
|\  
| * 94cb4df8 -- Resolve "Bump versions of agendas"
|/  
* 10664b3e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   335edd22 -- Merge branch 'develop' into 'master'
|\  
| * 117cdf64 -- Develop
|/  
* 66a67a89 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   bb2c4520 -- Merge branch '549-add-routing-for-run-visualization-tabs' into 'master'
|\  
| * c78c19a3 -- Resolve "Add routing for run visualization tabs"
|/  
* 410199da -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0d85b374 -- Merge branch 'develop' into 'master'
|\  
| * af863b0c -- Develop
|/  
* f092bd09 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0ae646c2 -- Merge branch 'develop' into 'master'
|\  
| * 0ff7f79e -- Create release candidate tag
|/  
* 31a8ec8f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a28b2dcb -- Merge branch 'fix-failing-pipeline' into 'master'
|\  
| * 743238e3 -- Fix failing pipeline
|/  
* 9f32a818 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   191c1c5a -- Merge branch '546-fix-failing-docker-push-pipeline-stage' into 'master'
|\  
| * 9ef9dcab -- Resolve "Fix failing docker push pipeline stage"
|/  
* b144800b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   6798b6a5 -- Merge branch '545-update-to-angular-13' into 'master'
|\  
| * 0452dbf7 -- Resolve "Update to Angular 13"
|/  
*   e6921b11 -- Merge branch '544-remove-tour-guide' into 'master'
|\  
| * 64eba553 -- Remove Tour guide
|/  
* 82d9c597 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   534273f0 -- Merge branch '543-bump-version' into 'master'
|\  
| * 66c4805a -- Bump version
|/  
* 1dc638f4 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   dcb7eea7 -- Merge branch '541-bump-version-of-sandbox-agenda' into 'master'
|\  
| * 577b9964 -- Resolve "Bump version of sandbox agenda"
|/  
* 9a5b7e4b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   95a7ea80 -- Merge branch '540-release-testing-fix-found-issues' into 'master'
|\  
| * 23d5291f -- Latest release
|/  
* 63200336 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   50ef18d5 -- Merge branch '539-release-new-version' into 'master'
|\  
| * 7987f0e0 -- Resolve "Release new version"
|/  
* fcfa85cb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   d853702e -- Merge branch '538-latest-versions-update' into 'master'
|\  
| * 45b66042 -- Resolve "Latest versions update"
|/  
* 56660793 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   792cfe4f -- Merge branch '536-release-2021-12-0' into 'master'
|\  
| * b5948d67 -- Resolve "Release 2021.12.0"
|/  
*   470388a4 -- Merge branch '534-add-license-file' into 'master'
|\  
| * 0a8ddb6d -- Add license file
|/  
* 1e2e474b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   43180dec -- Merge branch '532-update-version-of-the-sandbox-agenda-and-training-agenda-due-to-guacamole-integration' into 'master'
|\  
| * c882bb6d -- Itegration of the Apache Guacamole - bump versions of the topology graph, training agenda and sandbox agenda.
|/  
* 076c437a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 15.0.0 Update to Angular 15, update all corresponding agendas and their libraries.
* 61c98197 -- Add new ATD simulator with material styling
* 3b209326 -- Add fixed agendas
* f1c5768e -- Update all libraries to newest versions
* 0276655e -- Fix Docker node issue
* c908abbd -- Update packages to Angular 15
* cdc3928c -- Update mock DB
* bfc420cf -- Update to Angular 15
* f65a5e19 -- Updated versions of training agenda, api and model.
* 3adad9eb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 6fbfa717 -- Bump sandbox agenda library version.
* df7e818f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8fe29bdb -- Resolve "Update fixed agendas"
* 8bdf996e -- Update package versions
* b5070c37 -- Bump versions for bug fix release.
* 48692e28 -- Updated version and routing of cheating detection.
* 98562f12 -- Updated routing of components.
* 46711d11 -- Updated cheating detection routing modules.
* 92968cd2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* a97fd581 -- Develop
* e4be79b0 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9a8a0adc -- Merge develop into master
* 7e1bb505 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 415e8f2a -- Develop
* f3b97321 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* e040217f -- Develop
* 8ac83f44 -- Bump sandbox agenda and api
* afd3aea9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f83e2a8b -- Disable cheating detection
* 0f75542b -- Bump versions
* e15d64a5 -- Bump versions
* 33685b52 -- Resolve latest problems in sandbox agenda
* b0ee8a3b -- Added slider for sandbox allocation
* 475cc64a -- Added cheating detection. Disabled the routing as it is causing huge performance issues
* a9beb75a -- Bump sandbox agenda to enable sorting of the pool detail table
* 0f9182d0 -- Resolve problem regarding training run delete
* dbf12443 -- Fix build
* a55f43ef -- Bump sandbox uuid changes
* 8bd77ca3 -- Bump hurdling viz
* c47364f9 -- Bump walkthrough visualization to address problem with chrome responsibility
* cc8c5dee -- Fix integrity of package
* 87d71f17 -- Bump versions for stagining testing
* 9ebb79db -- Bump sandbox agenda - reflect changes in resource pages
* 63c9d005 -- Update to latest changes in training agenda and command visualization
* 05e8f948 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   c604188a -- Merge branch 'develop' into 'master'
|\  
| * 5e22e370 -- Address changes in sandbox agenda.
|/  
* 6e6ebb59 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   47aec793 -- Merge branch 'develop' into 'master'
|\  
| * f3566053 -- Develop
|/  
* 5ef6d630 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   5fe82f54 -- Merge branch 'develop' into 'master'
|\  
| * a271aa8a -- Develop
|/  
* e572094b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   61a36494 -- Merge branch 'develop' into 'master'
|\  
| * ebb054f5 -- Develop
|/  
* fb29c496 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   094e8462 -- Merge branch 'develop' into 'master'
|\  
| * d640d8a9 -- Merge develop into master
|/  
* 2f6a791a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a8f74328 -- Merge branch 'develop' into 'master'
|\  
| * 5b90d9e7 -- Develop
|/  
* 3af8daf2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   36caaa2d -- Merge branch '551-bump-patch-version-of-training-agenda' into 'master'
|\  
| * a4b7a473 -- Resolve "Bump patch version of training agenda"
|/  
* 862c05e6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   da9a99f8 -- Merge branch '550-bump-versions-of-agendas' into 'master'
|\  
| * 94cb4df8 -- Resolve "Bump versions of agendas"
|/  
* 10664b3e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   335edd22 -- Merge branch 'develop' into 'master'
|\  
| * 117cdf64 -- Develop
|/  
* 66a67a89 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   bb2c4520 -- Merge branch '549-add-routing-for-run-visualization-tabs' into 'master'
|\  
| * c78c19a3 -- Resolve "Add routing for run visualization tabs"
|/  
* 410199da -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0d85b374 -- Merge branch 'develop' into 'master'
|\  
| * af863b0c -- Develop
|/  
* f092bd09 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0ae646c2 -- Merge branch 'develop' into 'master'
|\  
| * 0ff7f79e -- Create release candidate tag
|/  
* 31a8ec8f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a28b2dcb -- Merge branch 'fix-failing-pipeline' into 'master'
|\  
| * 743238e3 -- Fix failing pipeline
|/  
* 9f32a818 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   191c1c5a -- Merge branch '546-fix-failing-docker-push-pipeline-stage' into 'master'
|\  
| * 9ef9dcab -- Resolve "Fix failing docker push pipeline stage"
|/  
* b144800b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   6798b6a5 -- Merge branch '545-update-to-angular-13' into 'master'
|\  
| * 0452dbf7 -- Resolve "Update to Angular 13"
|/  
*   e6921b11 -- Merge branch '544-remove-tour-guide' into 'master'
|\  
| * 64eba553 -- Remove Tour guide
|/  
* 82d9c597 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   534273f0 -- Merge branch '543-bump-version' into 'master'
|\  
| * 66c4805a -- Bump version
|/  
* 1dc638f4 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   dcb7eea7 -- Merge branch '541-bump-version-of-sandbox-agenda' into 'master'
|\  
| * 577b9964 -- Resolve "Bump version of sandbox agenda"
|/  
* 9a5b7e4b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   95a7ea80 -- Merge branch '540-release-testing-fix-found-issues' into 'master'
|\  
| * 23d5291f -- Latest release
|/  
* 63200336 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   50ef18d5 -- Merge branch '539-release-new-version' into 'master'
|\  
| * 7987f0e0 -- Resolve "Release new version"
|/  
* fcfa85cb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   d853702e -- Merge branch '538-latest-versions-update' into 'master'
|\  
| * 45b66042 -- Resolve "Latest versions update"
|/  
* 56660793 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   792cfe4f -- Merge branch '536-release-2021-12-0' into 'master'
|\  
| * b5948d67 -- Resolve "Release 2021.12.0"
|/  
*   470388a4 -- Merge branch '534-add-license-file' into 'master'
|\  
| * 0a8ddb6d -- Add license file
|/  
* 1e2e474b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   43180dec -- Merge branch '532-update-version-of-the-sandbox-agenda-and-training-agenda-due-to-guacamole-integration' into 'master'
|\  
| * c882bb6d -- Itegration of the Apache Guacamole - bump versions of the topology graph, training agenda and sandbox agenda.
|/  
* 076c437a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 14.2.7 Fix faulty sandbox-agenda allocation call for a single sandbox.
* 6fbfa717 -- Bump sandbox agenda library version.
* df7e818f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 8fe29bdb -- Resolve "Update fixed agendas"
* 8bdf996e -- Update package versions
* b5070c37 -- Bump versions for bug fix release.
* 48692e28 -- Updated version and routing of cheating detection.
* 98562f12 -- Updated routing of components.
* 46711d11 -- Updated cheating detection routing modules.
* 92968cd2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* a97fd581 -- Develop
* e4be79b0 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9a8a0adc -- Merge develop into master
* 7e1bb505 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 415e8f2a -- Develop
* f3b97321 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* e040217f -- Develop
* 8ac83f44 -- Bump sandbox agenda and api
* afd3aea9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f83e2a8b -- Disable cheating detection
* 0f75542b -- Bump versions
* e15d64a5 -- Bump versions
* 33685b52 -- Resolve latest problems in sandbox agenda
* b0ee8a3b -- Added slider for sandbox allocation
* 475cc64a -- Added cheating detection. Disabled the routing as it is causing huge performance issues
* a9beb75a -- Bump sandbox agenda to enable sorting of the pool detail table
* 0f9182d0 -- Resolve problem regarding training run delete
* dbf12443 -- Fix build
* a55f43ef -- Bump sandbox uuid changes
* 8bd77ca3 -- Bump hurdling viz
* c47364f9 -- Bump walkthrough visualization to address problem with chrome responsibility
* cc8c5dee -- Fix integrity of package
* 87d71f17 -- Bump versions for stagining testing
* 9ebb79db -- Bump sandbox agenda - reflect changes in resource pages
* 63c9d005 -- Update to latest changes in training agenda and command visualization
* 05e8f948 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   c604188a -- Merge branch 'develop' into 'master'
|\  
| * 5e22e370 -- Address changes in sandbox agenda.
|/  
* 6e6ebb59 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   47aec793 -- Merge branch 'develop' into 'master'
|\  
| * f3566053 -- Develop
|/  
* 5ef6d630 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   5fe82f54 -- Merge branch 'develop' into 'master'
|\  
| * a271aa8a -- Develop
|/  
* e572094b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   61a36494 -- Merge branch 'develop' into 'master'
|\  
| * ebb054f5 -- Develop
|/  
* fb29c496 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   094e8462 -- Merge branch 'develop' into 'master'
|\  
| * d640d8a9 -- Merge develop into master
|/  
* 2f6a791a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a8f74328 -- Merge branch 'develop' into 'master'
|\  
| * 5b90d9e7 -- Develop
|/  
* 3af8daf2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   36caaa2d -- Merge branch '551-bump-patch-version-of-training-agenda' into 'master'
|\  
| * a4b7a473 -- Resolve "Bump patch version of training agenda"
|/  
* 862c05e6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   da9a99f8 -- Merge branch '550-bump-versions-of-agendas' into 'master'
|\  
| * 94cb4df8 -- Resolve "Bump versions of agendas"
|/  
* 10664b3e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   335edd22 -- Merge branch 'develop' into 'master'
|\  
| * 117cdf64 -- Develop
|/  
* 66a67a89 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   bb2c4520 -- Merge branch '549-add-routing-for-run-visualization-tabs' into 'master'
|\  
| * c78c19a3 -- Resolve "Add routing for run visualization tabs"
|/  
* 410199da -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0d85b374 -- Merge branch 'develop' into 'master'
|\  
| * af863b0c -- Develop
|/  
* f092bd09 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0ae646c2 -- Merge branch 'develop' into 'master'
|\  
| * 0ff7f79e -- Create release candidate tag
|/  
* 31a8ec8f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a28b2dcb -- Merge branch 'fix-failing-pipeline' into 'master'
|\  
| * 743238e3 -- Fix failing pipeline
|/  
* 9f32a818 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   191c1c5a -- Merge branch '546-fix-failing-docker-push-pipeline-stage' into 'master'
|\  
| * 9ef9dcab -- Resolve "Fix failing docker push pipeline stage"
|/  
* b144800b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   6798b6a5 -- Merge branch '545-update-to-angular-13' into 'master'
|\  
| * 0452dbf7 -- Resolve "Update to Angular 13"
|/  
*   e6921b11 -- Merge branch '544-remove-tour-guide' into 'master'
|\  
| * 64eba553 -- Remove Tour guide
|/  
* 82d9c597 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   534273f0 -- Merge branch '543-bump-version' into 'master'
|\  
| * 66c4805a -- Bump version
|/  
* 1dc638f4 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   dcb7eea7 -- Merge branch '541-bump-version-of-sandbox-agenda' into 'master'
|\  
| * 577b9964 -- Resolve "Bump version of sandbox agenda"
|/  
* 9a5b7e4b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   95a7ea80 -- Merge branch '540-release-testing-fix-found-issues' into 'master'
|\  
| * 23d5291f -- Latest release
|/  
* 63200336 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   50ef18d5 -- Merge branch '539-release-new-version' into 'master'
|\  
| * 7987f0e0 -- Resolve "Release new version"
|/  
* fcfa85cb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   d853702e -- Merge branch '538-latest-versions-update' into 'master'
|\  
| * 45b66042 -- Resolve "Latest versions update"
|/  
* 56660793 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   792cfe4f -- Merge branch '536-release-2021-12-0' into 'master'
|\  
| * b5948d67 -- Resolve "Release 2021.12.0"
|/  
*   470388a4 -- Merge branch '534-add-license-file' into 'master'
|\  
| * 0a8ddb6d -- Add license file
|/  
* 1e2e474b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   43180dec -- Merge branch '532-update-version-of-the-sandbox-agenda-and-training-agenda-due-to-guacamole-integration' into 'master'
|\  
| * c882bb6d -- Itegration of the Apache Guacamole - bump versions of the topology graph, training agenda and sandbox agenda.
|/  
* 076c437a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 14.2.6 Update training, sandbox, and user-and-group agendas to address new bugfixes.
* 8fe29bdb -- Resolve "Update fixed agendas"
* 8bdf996e -- Update package versions
* b5070c37 -- Bump versions for bug fix release.
* 48692e28 -- Updated version and routing of cheating detection.
* 98562f12 -- Updated routing of components.
* 46711d11 -- Updated cheating detection routing modules.
* 92968cd2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* a97fd581 -- Develop
* e4be79b0 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9a8a0adc -- Merge develop into master
* 7e1bb505 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 415e8f2a -- Develop
* f3b97321 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* e040217f -- Develop
* 8ac83f44 -- Bump sandbox agenda and api
* afd3aea9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f83e2a8b -- Disable cheating detection
* 0f75542b -- Bump versions
* e15d64a5 -- Bump versions
* 33685b52 -- Resolve latest problems in sandbox agenda
* b0ee8a3b -- Added slider for sandbox allocation
* 475cc64a -- Added cheating detection. Disabled the routing as it is causing huge performance issues
* a9beb75a -- Bump sandbox agenda to enable sorting of the pool detail table
* 0f9182d0 -- Resolve problem regarding training run delete
* dbf12443 -- Fix build
* a55f43ef -- Bump sandbox uuid changes
* 8bd77ca3 -- Bump hurdling viz
* c47364f9 -- Bump walkthrough visualization to address problem with chrome responsibility
* cc8c5dee -- Fix integrity of package
* 87d71f17 -- Bump versions for stagining testing
* 9ebb79db -- Bump sandbox agenda - reflect changes in resource pages
* 63c9d005 -- Update to latest changes in training agenda and command visualization
* 05e8f948 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   c604188a -- Merge branch 'develop' into 'master'
|\  
| * 5e22e370 -- Address changes in sandbox agenda.
|/  
* 6e6ebb59 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   47aec793 -- Merge branch 'develop' into 'master'
|\  
| * f3566053 -- Develop
|/  
* 5ef6d630 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   5fe82f54 -- Merge branch 'develop' into 'master'
|\  
| * a271aa8a -- Develop
|/  
* e572094b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   61a36494 -- Merge branch 'develop' into 'master'
|\  
| * ebb054f5 -- Develop
|/  
* fb29c496 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   094e8462 -- Merge branch 'develop' into 'master'
|\  
| * d640d8a9 -- Merge develop into master
|/  
* 2f6a791a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a8f74328 -- Merge branch 'develop' into 'master'
|\  
| * 5b90d9e7 -- Develop
|/  
* 3af8daf2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   36caaa2d -- Merge branch '551-bump-patch-version-of-training-agenda' into 'master'
|\  
| * a4b7a473 -- Resolve "Bump patch version of training agenda"
|/  
* 862c05e6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   da9a99f8 -- Merge branch '550-bump-versions-of-agendas' into 'master'
|\  
| * 94cb4df8 -- Resolve "Bump versions of agendas"
|/  
* 10664b3e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   335edd22 -- Merge branch 'develop' into 'master'
|\  
| * 117cdf64 -- Develop
|/  
* 66a67a89 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   bb2c4520 -- Merge branch '549-add-routing-for-run-visualization-tabs' into 'master'
|\  
| * c78c19a3 -- Resolve "Add routing for run visualization tabs"
|/  
* 410199da -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0d85b374 -- Merge branch 'develop' into 'master'
|\  
| * af863b0c -- Develop
|/  
* f092bd09 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0ae646c2 -- Merge branch 'develop' into 'master'
|\  
| * 0ff7f79e -- Create release candidate tag
|/  
* 31a8ec8f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a28b2dcb -- Merge branch 'fix-failing-pipeline' into 'master'
|\  
| * 743238e3 -- Fix failing pipeline
|/  
* 9f32a818 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   191c1c5a -- Merge branch '546-fix-failing-docker-push-pipeline-stage' into 'master'
|\  
| * 9ef9dcab -- Resolve "Fix failing docker push pipeline stage"
|/  
* b144800b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   6798b6a5 -- Merge branch '545-update-to-angular-13' into 'master'
|\  
| * 0452dbf7 -- Resolve "Update to Angular 13"
|/  
*   e6921b11 -- Merge branch '544-remove-tour-guide' into 'master'
|\  
| * 64eba553 -- Remove Tour guide
|/  
* 82d9c597 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   534273f0 -- Merge branch '543-bump-version' into 'master'
|\  
| * 66c4805a -- Bump version
|/  
* 1dc638f4 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   dcb7eea7 -- Merge branch '541-bump-version-of-sandbox-agenda' into 'master'
|\  
| * 577b9964 -- Resolve "Bump version of sandbox agenda"
|/  
* 9a5b7e4b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   95a7ea80 -- Merge branch '540-release-testing-fix-found-issues' into 'master'
|\  
| * 23d5291f -- Latest release
|/  
* 63200336 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   50ef18d5 -- Merge branch '539-release-new-version' into 'master'
|\  
| * 7987f0e0 -- Resolve "Release new version"
|/  
* fcfa85cb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   d853702e -- Merge branch '538-latest-versions-update' into 'master'
|\  
| * 45b66042 -- Resolve "Latest versions update"
|/  
* 56660793 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   792cfe4f -- Merge branch '536-release-2021-12-0' into 'master'
|\  
| * b5948d67 -- Resolve "Release 2021.12.0"
|/  
*   470388a4 -- Merge branch '534-add-license-file' into 'master'
|\  
| * 0a8ddb6d -- Add license file
|/  
* 1e2e474b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   43180dec -- Merge branch '532-update-version-of-the-sandbox-agenda-and-training-agenda-due-to-guacamole-integration' into 'master'
|\  
| * c882bb6d -- Itegration of the Apache Guacamole - bump versions of the topology graph, training agenda and sandbox agenda.
|/  
* 076c437a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 14.2.5 Bump versions of training agenda, sandbox agenda and topology graph for bugfix release.
* a97fd581 -- Develop
* e4be79b0 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9a8a0adc -- Merge develop into master
* 7e1bb505 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 415e8f2a -- Develop
* f3b97321 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* e040217f -- Develop
* 8ac83f44 -- Bump sandbox agenda and api
* afd3aea9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f83e2a8b -- Disable cheating detection
* 0f75542b -- Bump versions
* e15d64a5 -- Bump versions
* 33685b52 -- Resolve latest problems in sandbox agenda
* b0ee8a3b -- Added slider for sandbox allocation
* 475cc64a -- Added cheating detection. Disabled the routing as it is causing huge performance issues
* a9beb75a -- Bump sandbox agenda to enable sorting of the pool detail table
* 0f9182d0 -- Resolve problem regarding training run delete
* dbf12443 -- Fix build
* a55f43ef -- Bump sandbox uuid changes
* 8bd77ca3 -- Bump hurdling viz
* c47364f9 -- Bump walkthrough visualization to address problem with chrome responsibility
* cc8c5dee -- Fix integrity of package
* 87d71f17 -- Bump versions for stagining testing
* 9ebb79db -- Bump sandbox agenda - reflect changes in resource pages
* 63c9d005 -- Update to latest changes in training agenda and command visualization
* 05e8f948 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   c604188a -- Merge branch 'develop' into 'master'
|\  
| * 5e22e370 -- Address changes in sandbox agenda.
|/  
* 6e6ebb59 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   47aec793 -- Merge branch 'develop' into 'master'
|\  
| * f3566053 -- Develop
|/  
* 5ef6d630 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   5fe82f54 -- Merge branch 'develop' into 'master'
|\  
| * a271aa8a -- Develop
|/  
* e572094b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   61a36494 -- Merge branch 'develop' into 'master'
|\  
| * ebb054f5 -- Develop
|/  
* fb29c496 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   094e8462 -- Merge branch 'develop' into 'master'
|\  
| * d640d8a9 -- Merge develop into master
|/  
* 2f6a791a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a8f74328 -- Merge branch 'develop' into 'master'
|\  
| * 5b90d9e7 -- Develop
|/  
* 3af8daf2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   36caaa2d -- Merge branch '551-bump-patch-version-of-training-agenda' into 'master'
|\  
| * a4b7a473 -- Resolve "Bump patch version of training agenda"
|/  
* 862c05e6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   da9a99f8 -- Merge branch '550-bump-versions-of-agendas' into 'master'
|\  
| * 94cb4df8 -- Resolve "Bump versions of agendas"
|/  
* 10664b3e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   335edd22 -- Merge branch 'develop' into 'master'
|\  
| * 117cdf64 -- Develop
|/  
* 66a67a89 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   bb2c4520 -- Merge branch '549-add-routing-for-run-visualization-tabs' into 'master'
|\  
| * c78c19a3 -- Resolve "Add routing for run visualization tabs"
|/  
* 410199da -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0d85b374 -- Merge branch 'develop' into 'master'
|\  
| * af863b0c -- Develop
|/  
* f092bd09 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0ae646c2 -- Merge branch 'develop' into 'master'
|\  
| * 0ff7f79e -- Create release candidate tag
|/  
* 31a8ec8f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a28b2dcb -- Merge branch 'fix-failing-pipeline' into 'master'
|\  
| * 743238e3 -- Fix failing pipeline
|/  
* 9f32a818 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   191c1c5a -- Merge branch '546-fix-failing-docker-push-pipeline-stage' into 'master'
|\  
| * 9ef9dcab -- Resolve "Fix failing docker push pipeline stage"
|/  
* b144800b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   6798b6a5 -- Merge branch '545-update-to-angular-13' into 'master'
|\  
| * 0452dbf7 -- Resolve "Update to Angular 13"
|/  
*   e6921b11 -- Merge branch '544-remove-tour-guide' into 'master'
|\  
| * 64eba553 -- Remove Tour guide
|/  
* 82d9c597 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   534273f0 -- Merge branch '543-bump-version' into 'master'
|\  
| * 66c4805a -- Bump version
|/  
* 1dc638f4 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   dcb7eea7 -- Merge branch '541-bump-version-of-sandbox-agenda' into 'master'
|\  
| * 577b9964 -- Resolve "Bump version of sandbox agenda"
|/  
* 9a5b7e4b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   95a7ea80 -- Merge branch '540-release-testing-fix-found-issues' into 'master'
|\  
| * 23d5291f -- Latest release
|/  
* 63200336 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   50ef18d5 -- Merge branch '539-release-new-version' into 'master'
|\  
| * 7987f0e0 -- Resolve "Release new version"
|/  
* fcfa85cb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   d853702e -- Merge branch '538-latest-versions-update' into 'master'
|\  
| * 45b66042 -- Resolve "Latest versions update"
|/  
* 56660793 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   792cfe4f -- Merge branch '536-release-2021-12-0' into 'master'
|\  
| * b5948d67 -- Resolve "Release 2021.12.0"
|/  
*   470388a4 -- Merge branch '534-add-license-file' into 'master'
|\  
| * 0a8ddb6d -- Add license file
|/  
* 1e2e474b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   43180dec -- Merge branch '532-update-version-of-the-sandbox-agenda-and-training-agenda-due-to-guacamole-integration' into 'master'
|\  
| * c882bb6d -- Itegration of the Apache Guacamole - bump versions of the topology graph, training agenda and sandbox agenda.
|/  
* 076c437a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 14.2.4 Bump version of training agenda for minor cheating detection fixes.
* 9a8a0adc -- Merge develop into master
* 7e1bb505 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 415e8f2a -- Develop
* f3b97321 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* e040217f -- Develop
* 8ac83f44 -- Bump sandbox agenda and api
* afd3aea9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f83e2a8b -- Disable cheating detection
* 0f75542b -- Bump versions
* e15d64a5 -- Bump versions
* 33685b52 -- Resolve latest problems in sandbox agenda
* b0ee8a3b -- Added slider for sandbox allocation
* 475cc64a -- Added cheating detection. Disabled the routing as it is causing huge performance issues
* a9beb75a -- Bump sandbox agenda to enable sorting of the pool detail table
* 0f9182d0 -- Resolve problem regarding training run delete
* dbf12443 -- Fix build
* a55f43ef -- Bump sandbox uuid changes
* 8bd77ca3 -- Bump hurdling viz
* c47364f9 -- Bump walkthrough visualization to address problem with chrome responsibility
* cc8c5dee -- Fix integrity of package
* 87d71f17 -- Bump versions for stagining testing
* 9ebb79db -- Bump sandbox agenda - reflect changes in resource pages
* 63c9d005 -- Update to latest changes in training agenda and command visualization
* 05e8f948 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   c604188a -- Merge branch 'develop' into 'master'
|\  
| * 5e22e370 -- Address changes in sandbox agenda.
|/  
* 6e6ebb59 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   47aec793 -- Merge branch 'develop' into 'master'
|\  
| * f3566053 -- Develop
|/  
* 5ef6d630 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   5fe82f54 -- Merge branch 'develop' into 'master'
|\  
| * a271aa8a -- Develop
|/  
* e572094b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   61a36494 -- Merge branch 'develop' into 'master'
|\  
| * ebb054f5 -- Develop
|/  
* fb29c496 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   094e8462 -- Merge branch 'develop' into 'master'
|\  
| * d640d8a9 -- Merge develop into master
|/  
* 2f6a791a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a8f74328 -- Merge branch 'develop' into 'master'
|\  
| * 5b90d9e7 -- Develop
|/  
* 3af8daf2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   36caaa2d -- Merge branch '551-bump-patch-version-of-training-agenda' into 'master'
|\  
| * a4b7a473 -- Resolve "Bump patch version of training agenda"
|/  
* 862c05e6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   da9a99f8 -- Merge branch '550-bump-versions-of-agendas' into 'master'
|\  
| * 94cb4df8 -- Resolve "Bump versions of agendas"
|/  
* 10664b3e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   335edd22 -- Merge branch 'develop' into 'master'
|\  
| * 117cdf64 -- Develop
|/  
* 66a67a89 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   bb2c4520 -- Merge branch '549-add-routing-for-run-visualization-tabs' into 'master'
|\  
| * c78c19a3 -- Resolve "Add routing for run visualization tabs"
|/  
* 410199da -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0d85b374 -- Merge branch 'develop' into 'master'
|\  
| * af863b0c -- Develop
|/  
* f092bd09 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0ae646c2 -- Merge branch 'develop' into 'master'
|\  
| * 0ff7f79e -- Create release candidate tag
|/  
* 31a8ec8f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a28b2dcb -- Merge branch 'fix-failing-pipeline' into 'master'
|\  
| * 743238e3 -- Fix failing pipeline
|/  
* 9f32a818 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   191c1c5a -- Merge branch '546-fix-failing-docker-push-pipeline-stage' into 'master'
|\  
| * 9ef9dcab -- Resolve "Fix failing docker push pipeline stage"
|/  
* b144800b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   6798b6a5 -- Merge branch '545-update-to-angular-13' into 'master'
|\  
| * 0452dbf7 -- Resolve "Update to Angular 13"
|/  
*   e6921b11 -- Merge branch '544-remove-tour-guide' into 'master'
|\  
| * 64eba553 -- Remove Tour guide
|/  
* 82d9c597 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   534273f0 -- Merge branch '543-bump-version' into 'master'
|\  
| * 66c4805a -- Bump version
|/  
* 1dc638f4 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   dcb7eea7 -- Merge branch '541-bump-version-of-sandbox-agenda' into 'master'
|\  
| * 577b9964 -- Resolve "Bump version of sandbox agenda"
|/  
* 9a5b7e4b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   95a7ea80 -- Merge branch '540-release-testing-fix-found-issues' into 'master'
|\  
| * 23d5291f -- Latest release
|/  
* 63200336 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   50ef18d5 -- Merge branch '539-release-new-version' into 'master'
|\  
| * 7987f0e0 -- Resolve "Release new version"
|/  
* fcfa85cb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   d853702e -- Merge branch '538-latest-versions-update' into 'master'
|\  
| * 45b66042 -- Resolve "Latest versions update"
|/  
* 56660793 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   792cfe4f -- Merge branch '536-release-2021-12-0' into 'master'
|\  
| * b5948d67 -- Resolve "Release 2021.12.0"
|/  
*   470388a4 -- Merge branch '534-add-license-file' into 'master'
|\  
| * 0a8ddb6d -- Add license file
|/  
* 1e2e474b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   43180dec -- Merge branch '532-update-version-of-the-sandbox-agenda-and-training-agenda-due-to-guacamole-integration' into 'master'
|\  
| * c882bb6d -- Itegration of the Apache Guacamole - bump versions of the topology graph, training agenda and sandbox agenda.
|/  
* 076c437a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 14.1.5 Bump version of topology model and update topology assets.
* 415e8f2a -- Develop
* f3b97321 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* e040217f -- Develop
* 8ac83f44 -- Bump sandbox agenda and api
* afd3aea9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f83e2a8b -- Disable cheating detection
* 0f75542b -- Bump versions
* e15d64a5 -- Bump versions
* 33685b52 -- Resolve latest problems in sandbox agenda
* b0ee8a3b -- Added slider for sandbox allocation
* 475cc64a -- Added cheating detection. Disabled the routing as it is causing huge performance issues
* a9beb75a -- Bump sandbox agenda to enable sorting of the pool detail table
* 0f9182d0 -- Resolve problem regarding training run delete
* dbf12443 -- Fix build
* a55f43ef -- Bump sandbox uuid changes
* 8bd77ca3 -- Bump hurdling viz
* c47364f9 -- Bump walkthrough visualization to address problem with chrome responsibility
* cc8c5dee -- Fix integrity of package
* 87d71f17 -- Bump versions for stagining testing
* 9ebb79db -- Bump sandbox agenda - reflect changes in resource pages
* 63c9d005 -- Update to latest changes in training agenda and command visualization
* 05e8f948 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   c604188a -- Merge branch 'develop' into 'master'
|\  
| * 5e22e370 -- Address changes in sandbox agenda.
|/  
* 6e6ebb59 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   47aec793 -- Merge branch 'develop' into 'master'
|\  
| * f3566053 -- Develop
|/  
* 5ef6d630 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   5fe82f54 -- Merge branch 'develop' into 'master'
|\  
| * a271aa8a -- Develop
|/  
* e572094b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   61a36494 -- Merge branch 'develop' into 'master'
|\  
| * ebb054f5 -- Develop
|/  
* fb29c496 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   094e8462 -- Merge branch 'develop' into 'master'
|\  
| * d640d8a9 -- Merge develop into master
|/  
* 2f6a791a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a8f74328 -- Merge branch 'develop' into 'master'
|\  
| * 5b90d9e7 -- Develop
|/  
* 3af8daf2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   36caaa2d -- Merge branch '551-bump-patch-version-of-training-agenda' into 'master'
|\  
| * a4b7a473 -- Resolve "Bump patch version of training agenda"
|/  
* 862c05e6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   da9a99f8 -- Merge branch '550-bump-versions-of-agendas' into 'master'
|\  
| * 94cb4df8 -- Resolve "Bump versions of agendas"
|/  
* 10664b3e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   335edd22 -- Merge branch 'develop' into 'master'
|\  
| * 117cdf64 -- Develop
|/  
* 66a67a89 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   bb2c4520 -- Merge branch '549-add-routing-for-run-visualization-tabs' into 'master'
|\  
| * c78c19a3 -- Resolve "Add routing for run visualization tabs"
|/  
* 410199da -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0d85b374 -- Merge branch 'develop' into 'master'
|\  
| * af863b0c -- Develop
|/  
* f092bd09 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0ae646c2 -- Merge branch 'develop' into 'master'
|\  
| * 0ff7f79e -- Create release candidate tag
|/  
* 31a8ec8f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a28b2dcb -- Merge branch 'fix-failing-pipeline' into 'master'
|\  
| * 743238e3 -- Fix failing pipeline
|/  
* 9f32a818 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   191c1c5a -- Merge branch '546-fix-failing-docker-push-pipeline-stage' into 'master'
|\  
| * 9ef9dcab -- Resolve "Fix failing docker push pipeline stage"
|/  
* b144800b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   6798b6a5 -- Merge branch '545-update-to-angular-13' into 'master'
|\  
| * 0452dbf7 -- Resolve "Update to Angular 13"
|/  
*   e6921b11 -- Merge branch '544-remove-tour-guide' into 'master'
|\  
| * 64eba553 -- Remove Tour guide
|/  
* 82d9c597 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   534273f0 -- Merge branch '543-bump-version' into 'master'
|\  
| * 66c4805a -- Bump version
|/  
* 1dc638f4 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   dcb7eea7 -- Merge branch '541-bump-version-of-sandbox-agenda' into 'master'
|\  
| * 577b9964 -- Resolve "Bump version of sandbox agenda"
|/  
* 9a5b7e4b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   95a7ea80 -- Merge branch '540-release-testing-fix-found-issues' into 'master'
|\  
| * 23d5291f -- Latest release
|/  
* 63200336 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   50ef18d5 -- Merge branch '539-release-new-version' into 'master'
|\  
| * 7987f0e0 -- Resolve "Release new version"
|/  
* fcfa85cb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   d853702e -- Merge branch '538-latest-versions-update' into 'master'
|\  
| * 45b66042 -- Resolve "Latest versions update"
|/  
* 56660793 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   792cfe4f -- Merge branch '536-release-2021-12-0' into 'master'
|\  
| * b5948d67 -- Resolve "Release 2021.12.0"
|/  
*   470388a4 -- Merge branch '534-add-license-file' into 'master'
|\  
| * 0a8ddb6d -- Add license file
|/  
* 1e2e474b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   43180dec -- Merge branch '532-update-version-of-the-sandbox-agenda-and-training-agenda-due-to-guacamole-integration' into 'master'
|\  
| * c882bb6d -- Itegration of the Apache Guacamole - bump versions of the topology graph, training agenda and sandbox agenda.
|/  
* 076c437a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 14.1.4 Bump version of training agenda, api and model, sandbox api, hurdling visualizations, angular topology.
* e040217f -- Develop
* 8ac83f44 -- Bump sandbox agenda and api
* afd3aea9 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* f83e2a8b -- Disable cheating detection
* 0f75542b -- Bump versions
* e15d64a5 -- Bump versions
* 33685b52 -- Resolve latest problems in sandbox agenda
* b0ee8a3b -- Added slider for sandbox allocation
* 475cc64a -- Added cheating detection. Disabled the routing as it is causing huge performance issues
* a9beb75a -- Bump sandbox agenda to enable sorting of the pool detail table
* 0f9182d0 -- Resolve problem regarding training run delete
* dbf12443 -- Fix build
* a55f43ef -- Bump sandbox uuid changes
* 8bd77ca3 -- Bump hurdling viz
* c47364f9 -- Bump walkthrough visualization to address problem with chrome responsibility
* cc8c5dee -- Fix integrity of package
* 87d71f17 -- Bump versions for stagining testing
* 9ebb79db -- Bump sandbox agenda - reflect changes in resource pages
* 63c9d005 -- Update to latest changes in training agenda and command visualization
* 05e8f948 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   c604188a -- Merge branch 'develop' into 'master'
|\  
| * 5e22e370 -- Address changes in sandbox agenda.
|/  
* 6e6ebb59 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   47aec793 -- Merge branch 'develop' into 'master'
|\  
| * f3566053 -- Develop
|/  
* 5ef6d630 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   5fe82f54 -- Merge branch 'develop' into 'master'
|\  
| * a271aa8a -- Develop
|/  
* e572094b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   61a36494 -- Merge branch 'develop' into 'master'
|\  
| * ebb054f5 -- Develop
|/  
* fb29c496 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   094e8462 -- Merge branch 'develop' into 'master'
|\  
| * d640d8a9 -- Merge develop into master
|/  
* 2f6a791a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a8f74328 -- Merge branch 'develop' into 'master'
|\  
| * 5b90d9e7 -- Develop
|/  
* 3af8daf2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   36caaa2d -- Merge branch '551-bump-patch-version-of-training-agenda' into 'master'
|\  
| * a4b7a473 -- Resolve "Bump patch version of training agenda"
|/  
* 862c05e6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   da9a99f8 -- Merge branch '550-bump-versions-of-agendas' into 'master'
|\  
| * 94cb4df8 -- Resolve "Bump versions of agendas"
|/  
* 10664b3e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   335edd22 -- Merge branch 'develop' into 'master'
|\  
| * 117cdf64 -- Develop
|/  
* 66a67a89 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   bb2c4520 -- Merge branch '549-add-routing-for-run-visualization-tabs' into 'master'
|\  
| * c78c19a3 -- Resolve "Add routing for run visualization tabs"
|/  
* 410199da -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0d85b374 -- Merge branch 'develop' into 'master'
|\  
| * af863b0c -- Develop
|/  
* f092bd09 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0ae646c2 -- Merge branch 'develop' into 'master'
|\  
| * 0ff7f79e -- Create release candidate tag
|/  
* 31a8ec8f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a28b2dcb -- Merge branch 'fix-failing-pipeline' into 'master'
|\  
| * 743238e3 -- Fix failing pipeline
|/  
* 9f32a818 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   191c1c5a -- Merge branch '546-fix-failing-docker-push-pipeline-stage' into 'master'
|\  
| * 9ef9dcab -- Resolve "Fix failing docker push pipeline stage"
|/  
* b144800b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   6798b6a5 -- Merge branch '545-update-to-angular-13' into 'master'
|\  
| * 0452dbf7 -- Resolve "Update to Angular 13"
|/  
*   e6921b11 -- Merge branch '544-remove-tour-guide' into 'master'
|\  
| * 64eba553 -- Remove Tour guide
|/  
* 82d9c597 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   534273f0 -- Merge branch '543-bump-version' into 'master'
|\  
| * 66c4805a -- Bump version
|/  
* 1dc638f4 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   dcb7eea7 -- Merge branch '541-bump-version-of-sandbox-agenda' into 'master'
|\  
| * 577b9964 -- Resolve "Bump version of sandbox agenda"
|/  
* 9a5b7e4b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   95a7ea80 -- Merge branch '540-release-testing-fix-found-issues' into 'master'
|\  
| * 23d5291f -- Latest release
|/  
* 63200336 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   50ef18d5 -- Merge branch '539-release-new-version' into 'master'
|\  
| * 7987f0e0 -- Resolve "Release new version"
|/  
* fcfa85cb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   d853702e -- Merge branch '538-latest-versions-update' into 'master'
|\  
| * 45b66042 -- Resolve "Latest versions update"
|/  
* 56660793 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   792cfe4f -- Merge branch '536-release-2021-12-0' into 'master'
|\  
| * b5948d67 -- Resolve "Release 2021.12.0"
|/  
*   470388a4 -- Merge branch '534-add-license-file' into 'master'
|\  
| * 0a8ddb6d -- Add license file
|/  
* 1e2e474b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   43180dec -- Merge branch '532-update-version-of-the-sandbox-agenda-and-training-agenda-due-to-guacamole-integration' into 'master'
|\  
| * c882bb6d -- Itegration of the Apache Guacamole - bump versions of the topology graph, training agenda and sandbox agenda.
|/  
* 076c437a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 22.12-rc.1 Release candidate for the 22.12 release.
* f83e2a8b -- Disable cheating detection
* 0f75542b -- Bump versions
* e15d64a5 -- Bump versions
* 33685b52 -- Resolve latest problems in sandbox agenda
* b0ee8a3b -- Added slider for sandbox allocation
* 475cc64a -- Added cheating detection. Disabled the routing as it is causing huge performance issues
* a9beb75a -- Bump sandbox agenda to enable sorting of the pool detail table
* 0f9182d0 -- Resolve problem regarding training run delete
* dbf12443 -- Fix build
* a55f43ef -- Bump sandbox uuid changes
* 8bd77ca3 -- Bump hurdling viz
* c47364f9 -- Bump walkthrough visualization to address problem with chrome responsibility
* cc8c5dee -- Fix integrity of package
* 87d71f17 -- Bump versions for stagining testing
* 9ebb79db -- Bump sandbox agenda - reflect changes in resource pages
* 63c9d005 -- Update to latest changes in training agenda and command visualization
* 05e8f948 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   c604188a -- Merge branch 'develop' into 'master'
|\  
| * 5e22e370 -- Address changes in sandbox agenda.
|/  
* 6e6ebb59 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   47aec793 -- Merge branch 'develop' into 'master'
|\  
| * f3566053 -- Develop
|/  
* 5ef6d630 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   5fe82f54 -- Merge branch 'develop' into 'master'
|\  
| * a271aa8a -- Develop
|/  
* e572094b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   61a36494 -- Merge branch 'develop' into 'master'
|\  
| * ebb054f5 -- Develop
|/  
* fb29c496 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   094e8462 -- Merge branch 'develop' into 'master'
|\  
| * d640d8a9 -- Merge develop into master
|/  
* 2f6a791a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a8f74328 -- Merge branch 'develop' into 'master'
|\  
| * 5b90d9e7 -- Develop
|/  
* 3af8daf2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   36caaa2d -- Merge branch '551-bump-patch-version-of-training-agenda' into 'master'
|\  
| * a4b7a473 -- Resolve "Bump patch version of training agenda"
|/  
* 862c05e6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   da9a99f8 -- Merge branch '550-bump-versions-of-agendas' into 'master'
|\  
| * 94cb4df8 -- Resolve "Bump versions of agendas"
|/  
* 10664b3e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   335edd22 -- Merge branch 'develop' into 'master'
|\  
| * 117cdf64 -- Develop
|/  
* 66a67a89 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   bb2c4520 -- Merge branch '549-add-routing-for-run-visualization-tabs' into 'master'
|\  
| * c78c19a3 -- Resolve "Add routing for run visualization tabs"
|/  
* 410199da -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0d85b374 -- Merge branch 'develop' into 'master'
|\  
| * af863b0c -- Develop
|/  
* f092bd09 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0ae646c2 -- Merge branch 'develop' into 'master'
|\  
| * 0ff7f79e -- Create release candidate tag
|/  
* 31a8ec8f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a28b2dcb -- Merge branch 'fix-failing-pipeline' into 'master'
|\  
| * 743238e3 -- Fix failing pipeline
|/  
* 9f32a818 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   191c1c5a -- Merge branch '546-fix-failing-docker-push-pipeline-stage' into 'master'
|\  
| * 9ef9dcab -- Resolve "Fix failing docker push pipeline stage"
|/  
* b144800b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   6798b6a5 -- Merge branch '545-update-to-angular-13' into 'master'
|\  
| * 0452dbf7 -- Resolve "Update to Angular 13"
|/  
*   e6921b11 -- Merge branch '544-remove-tour-guide' into 'master'
|\  
| * 64eba553 -- Remove Tour guide
|/  
* 82d9c597 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   534273f0 -- Merge branch '543-bump-version' into 'master'
|\  
| * 66c4805a -- Bump version
|/  
* 1dc638f4 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   dcb7eea7 -- Merge branch '541-bump-version-of-sandbox-agenda' into 'master'
|\  
| * 577b9964 -- Resolve "Bump version of sandbox agenda"
|/  
* 9a5b7e4b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   95a7ea80 -- Merge branch '540-release-testing-fix-found-issues' into 'master'
|\  
| * 23d5291f -- Latest release
|/  
* 63200336 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   50ef18d5 -- Merge branch '539-release-new-version' into 'master'
|\  
| * 7987f0e0 -- Resolve "Release new version"
|/  
* fcfa85cb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   d853702e -- Merge branch '538-latest-versions-update' into 'master'
|\  
| * 45b66042 -- Resolve "Latest versions update"
|/  
* 56660793 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   792cfe4f -- Merge branch '536-release-2021-12-0' into 'master'
|\  
| * b5948d67 -- Resolve "Release 2021.12.0"
|/  
*   470388a4 -- Merge branch '534-add-license-file' into 'master'
|\  
| * 0a8ddb6d -- Add license file
|/  
* 1e2e474b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   43180dec -- Merge branch '532-update-version-of-the-sandbox-agenda-and-training-agenda-due-to-guacamole-integration' into 'master'
|\  
| * c882bb6d -- Itegration of the Apache Guacamole - bump versions of the topology graph, training agenda and sandbox agenda.
|/  
* 076c437a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 14.1.3 Address changes in sandbox agenda.
*   c604188a -- Merge branch 'develop' into 'master'
|\  
| * 5e22e370 -- Address changes in sandbox agenda.
|/  
* 6e6ebb59 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   47aec793 -- Merge branch 'develop' into 'master'
|\  
| * f3566053 -- Develop
|/  
* 5ef6d630 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   5fe82f54 -- Merge branch 'develop' into 'master'
|\  
| * a271aa8a -- Develop
|/  
* e572094b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   61a36494 -- Merge branch 'develop' into 'master'
|\  
| * ebb054f5 -- Develop
|/  
* fb29c496 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   094e8462 -- Merge branch 'develop' into 'master'
|\  
| * d640d8a9 -- Merge develop into master
|/  
* 2f6a791a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a8f74328 -- Merge branch 'develop' into 'master'
|\  
| * 5b90d9e7 -- Develop
|/  
* 3af8daf2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   36caaa2d -- Merge branch '551-bump-patch-version-of-training-agenda' into 'master'
|\  
| * a4b7a473 -- Resolve "Bump patch version of training agenda"
|/  
* 862c05e6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   da9a99f8 -- Merge branch '550-bump-versions-of-agendas' into 'master'
|\  
| * 94cb4df8 -- Resolve "Bump versions of agendas"
|/  
* 10664b3e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   335edd22 -- Merge branch 'develop' into 'master'
|\  
| * 117cdf64 -- Develop
|/  
* 66a67a89 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   bb2c4520 -- Merge branch '549-add-routing-for-run-visualization-tabs' into 'master'
|\  
| * c78c19a3 -- Resolve "Add routing for run visualization tabs"
|/  
* 410199da -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0d85b374 -- Merge branch 'develop' into 'master'
|\  
| * af863b0c -- Develop
|/  
* f092bd09 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0ae646c2 -- Merge branch 'develop' into 'master'
|\  
| * 0ff7f79e -- Create release candidate tag
|/  
* 31a8ec8f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a28b2dcb -- Merge branch 'fix-failing-pipeline' into 'master'
|\  
| * 743238e3 -- Fix failing pipeline
|/  
* 9f32a818 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   191c1c5a -- Merge branch '546-fix-failing-docker-push-pipeline-stage' into 'master'
|\  
| * 9ef9dcab -- Resolve "Fix failing docker push pipeline stage"
|/  
* b144800b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   6798b6a5 -- Merge branch '545-update-to-angular-13' into 'master'
|\  
| * 0452dbf7 -- Resolve "Update to Angular 13"
|/  
*   e6921b11 -- Merge branch '544-remove-tour-guide' into 'master'
|\  
| * 64eba553 -- Remove Tour guide
|/  
* 82d9c597 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   534273f0 -- Merge branch '543-bump-version' into 'master'
|\  
| * 66c4805a -- Bump version
|/  
* 1dc638f4 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   dcb7eea7 -- Merge branch '541-bump-version-of-sandbox-agenda' into 'master'
|\  
| * 577b9964 -- Resolve "Bump version of sandbox agenda"
|/  
* 9a5b7e4b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   95a7ea80 -- Merge branch '540-release-testing-fix-found-issues' into 'master'
|\  
| * 23d5291f -- Latest release
|/  
* 63200336 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   50ef18d5 -- Merge branch '539-release-new-version' into 'master'
|\  
| * 7987f0e0 -- Resolve "Release new version"
|/  
* fcfa85cb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   d853702e -- Merge branch '538-latest-versions-update' into 'master'
|\  
| * 45b66042 -- Resolve "Latest versions update"
|/  
* 56660793 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   792cfe4f -- Merge branch '536-release-2021-12-0' into 'master'
|\  
| * b5948d67 -- Resolve "Release 2021.12.0"
|/  
*   470388a4 -- Merge branch '534-add-license-file' into 'master'
|\  
| * 0a8ddb6d -- Add license file
|/  
* 1e2e474b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   43180dec -- Merge branch '532-update-version-of-the-sandbox-agenda-and-training-agenda-due-to-guacamole-integration' into 'master'
|\  
| * c882bb6d -- Itegration of the Apache Guacamole - bump versions of the topology graph, training agenda and sandbox agenda.
|/  
* 076c437a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 14.1.2 Bump sandbox agenda to address issues with polling.
*   47aec793 -- Merge branch 'develop' into 'master'
|\  
| * f3566053 -- Develop
|/  
* 5ef6d630 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   5fe82f54 -- Merge branch 'develop' into 'master'
|\  
| * a271aa8a -- Develop
|/  
* e572094b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   61a36494 -- Merge branch 'develop' into 'master'
|\  
| * ebb054f5 -- Develop
|/  
* fb29c496 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   094e8462 -- Merge branch 'develop' into 'master'
|\  
| * d640d8a9 -- Merge develop into master
|/  
* 2f6a791a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a8f74328 -- Merge branch 'develop' into 'master'
|\  
| * 5b90d9e7 -- Develop
|/  
* 3af8daf2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   36caaa2d -- Merge branch '551-bump-patch-version-of-training-agenda' into 'master'
|\  
| * a4b7a473 -- Resolve "Bump patch version of training agenda"
|/  
* 862c05e6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   da9a99f8 -- Merge branch '550-bump-versions-of-agendas' into 'master'
|\  
| * 94cb4df8 -- Resolve "Bump versions of agendas"
|/  
* 10664b3e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   335edd22 -- Merge branch 'develop' into 'master'
|\  
| * 117cdf64 -- Develop
|/  
* 66a67a89 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   bb2c4520 -- Merge branch '549-add-routing-for-run-visualization-tabs' into 'master'
|\  
| * c78c19a3 -- Resolve "Add routing for run visualization tabs"
|/  
* 410199da -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0d85b374 -- Merge branch 'develop' into 'master'
|\  
| * af863b0c -- Develop
|/  
* f092bd09 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0ae646c2 -- Merge branch 'develop' into 'master'
|\  
| * 0ff7f79e -- Create release candidate tag
|/  
* 31a8ec8f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a28b2dcb -- Merge branch 'fix-failing-pipeline' into 'master'
|\  
| * 743238e3 -- Fix failing pipeline
|/  
* 9f32a818 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   191c1c5a -- Merge branch '546-fix-failing-docker-push-pipeline-stage' into 'master'
|\  
| * 9ef9dcab -- Resolve "Fix failing docker push pipeline stage"
|/  
* b144800b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   6798b6a5 -- Merge branch '545-update-to-angular-13' into 'master'
|\  
| * 0452dbf7 -- Resolve "Update to Angular 13"
|/  
*   e6921b11 -- Merge branch '544-remove-tour-guide' into 'master'
|\  
| * 64eba553 -- Remove Tour guide
|/  
* 82d9c597 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   534273f0 -- Merge branch '543-bump-version' into 'master'
|\  
| * 66c4805a -- Bump version
|/  
* 1dc638f4 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   dcb7eea7 -- Merge branch '541-bump-version-of-sandbox-agenda' into 'master'
|\  
| * 577b9964 -- Resolve "Bump version of sandbox agenda"
|/  
* 9a5b7e4b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   95a7ea80 -- Merge branch '540-release-testing-fix-found-issues' into 'master'
|\  
| * 23d5291f -- Latest release
|/  
* 63200336 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   50ef18d5 -- Merge branch '539-release-new-version' into 'master'
|\  
| * 7987f0e0 -- Resolve "Release new version"
|/  
* fcfa85cb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   d853702e -- Merge branch '538-latest-versions-update' into 'master'
|\  
| * 45b66042 -- Resolve "Latest versions update"
|/  
* 56660793 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   792cfe4f -- Merge branch '536-release-2021-12-0' into 'master'
|\  
| * b5948d67 -- Resolve "Release 2021.12.0"
|/  
*   470388a4 -- Merge branch '534-add-license-file' into 'master'
|\  
| * 0a8ddb6d -- Add license file
|/  
* 1e2e474b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   43180dec -- Merge branch '532-update-version-of-the-sandbox-agenda-and-training-agenda-due-to-guacamole-integration' into 'master'
|\  
| * c882bb6d -- Itegration of the Apache Guacamole - bump versions of the topology graph, training agenda and sandbox agenda.
|/  
* 076c437a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 14.1.1 Bump sandbox agenda to resolve problem with sandbox instance delete.
*   5fe82f54 -- Merge branch 'develop' into 'master'
|\  
| * a271aa8a -- Develop
|/  
* e572094b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   61a36494 -- Merge branch 'develop' into 'master'
|\  
| * ebb054f5 -- Develop
|/  
* fb29c496 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   094e8462 -- Merge branch 'develop' into 'master'
|\  
| * d640d8a9 -- Merge develop into master
|/  
* 2f6a791a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a8f74328 -- Merge branch 'develop' into 'master'
|\  
| * 5b90d9e7 -- Develop
|/  
* 3af8daf2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   36caaa2d -- Merge branch '551-bump-patch-version-of-training-agenda' into 'master'
|\  
| * a4b7a473 -- Resolve "Bump patch version of training agenda"
|/  
* 862c05e6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   da9a99f8 -- Merge branch '550-bump-versions-of-agendas' into 'master'
|\  
| * 94cb4df8 -- Resolve "Bump versions of agendas"
|/  
* 10664b3e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   335edd22 -- Merge branch 'develop' into 'master'
|\  
| * 117cdf64 -- Develop
|/  
* 66a67a89 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   bb2c4520 -- Merge branch '549-add-routing-for-run-visualization-tabs' into 'master'
|\  
| * c78c19a3 -- Resolve "Add routing for run visualization tabs"
|/  
* 410199da -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0d85b374 -- Merge branch 'develop' into 'master'
|\  
| * af863b0c -- Develop
|/  
* f092bd09 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0ae646c2 -- Merge branch 'develop' into 'master'
|\  
| * 0ff7f79e -- Create release candidate tag
|/  
* 31a8ec8f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a28b2dcb -- Merge branch 'fix-failing-pipeline' into 'master'
|\  
| * 743238e3 -- Fix failing pipeline
|/  
* 9f32a818 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   191c1c5a -- Merge branch '546-fix-failing-docker-push-pipeline-stage' into 'master'
|\  
| * 9ef9dcab -- Resolve "Fix failing docker push pipeline stage"
|/  
* b144800b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   6798b6a5 -- Merge branch '545-update-to-angular-13' into 'master'
|\  
| * 0452dbf7 -- Resolve "Update to Angular 13"
|/  
*   e6921b11 -- Merge branch '544-remove-tour-guide' into 'master'
|\  
| * 64eba553 -- Remove Tour guide
|/  
* 82d9c597 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   534273f0 -- Merge branch '543-bump-version' into 'master'
|\  
| * 66c4805a -- Bump version
|/  
* 1dc638f4 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   dcb7eea7 -- Merge branch '541-bump-version-of-sandbox-agenda' into 'master'
|\  
| * 577b9964 -- Resolve "Bump version of sandbox agenda"
|/  
* 9a5b7e4b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   95a7ea80 -- Merge branch '540-release-testing-fix-found-issues' into 'master'
|\  
| * 23d5291f -- Latest release
|/  
* 63200336 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   50ef18d5 -- Merge branch '539-release-new-version' into 'master'
|\  
| * 7987f0e0 -- Resolve "Release new version"
|/  
* fcfa85cb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   d853702e -- Merge branch '538-latest-versions-update' into 'master'
|\  
| * 45b66042 -- Resolve "Latest versions update"
|/  
* 56660793 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   792cfe4f -- Merge branch '536-release-2021-12-0' into 'master'
|\  
| * b5948d67 -- Resolve "Release 2021.12.0"
|/  
*   470388a4 -- Merge branch '534-add-license-file' into 'master'
|\  
| * 0a8ddb6d -- Add license file
|/  
* 1e2e474b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   43180dec -- Merge branch '532-update-version-of-the-sandbox-agenda-and-training-agenda-due-to-guacamole-integration' into 'master'
|\  
| * c882bb6d -- Itegration of the Apache Guacamole - bump versions of the topology graph, training agenda and sandbox agenda.
|/  
* 076c437a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 14.1.0 Add an option to export score of all trainees from a training instance. Change save strategy for a training instance with non-local environment. Optimize API calls, polling in sandbox agenda and move spice console loading to topology.
*   61a36494 -- Merge branch 'develop' into 'master'
|\  
| * ebb054f5 -- Develop
|/  
* fb29c496 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   094e8462 -- Merge branch 'develop' into 'master'
|\  
| * d640d8a9 -- Merge develop into master
|/  
* 2f6a791a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a8f74328 -- Merge branch 'develop' into 'master'
|\  
| * 5b90d9e7 -- Develop
|/  
* 3af8daf2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   36caaa2d -- Merge branch '551-bump-patch-version-of-training-agenda' into 'master'
|\  
| * a4b7a473 -- Resolve "Bump patch version of training agenda"
|/  
* 862c05e6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   da9a99f8 -- Merge branch '550-bump-versions-of-agendas' into 'master'
|\  
| * 94cb4df8 -- Resolve "Bump versions of agendas"
|/  
* 10664b3e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   335edd22 -- Merge branch 'develop' into 'master'
|\  
| * 117cdf64 -- Develop
|/  
* 66a67a89 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   bb2c4520 -- Merge branch '549-add-routing-for-run-visualization-tabs' into 'master'
|\  
| * c78c19a3 -- Resolve "Add routing for run visualization tabs"
|/  
* 410199da -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0d85b374 -- Merge branch 'develop' into 'master'
|\  
| * af863b0c -- Develop
|/  
* f092bd09 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0ae646c2 -- Merge branch 'develop' into 'master'
|\  
| * 0ff7f79e -- Create release candidate tag
|/  
* 31a8ec8f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a28b2dcb -- Merge branch 'fix-failing-pipeline' into 'master'
|\  
| * 743238e3 -- Fix failing pipeline
|/  
* 9f32a818 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   191c1c5a -- Merge branch '546-fix-failing-docker-push-pipeline-stage' into 'master'
|\  
| * 9ef9dcab -- Resolve "Fix failing docker push pipeline stage"
|/  
* b144800b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   6798b6a5 -- Merge branch '545-update-to-angular-13' into 'master'
|\  
| * 0452dbf7 -- Resolve "Update to Angular 13"
|/  
*   e6921b11 -- Merge branch '544-remove-tour-guide' into 'master'
|\  
| * 64eba553 -- Remove Tour guide
|/  
* 82d9c597 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   534273f0 -- Merge branch '543-bump-version' into 'master'
|\  
| * 66c4805a -- Bump version
|/  
* 1dc638f4 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   dcb7eea7 -- Merge branch '541-bump-version-of-sandbox-agenda' into 'master'
|\  
| * 577b9964 -- Resolve "Bump version of sandbox agenda"
|/  
* 9a5b7e4b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   95a7ea80 -- Merge branch '540-release-testing-fix-found-issues' into 'master'
|\  
| * 23d5291f -- Latest release
|/  
* 63200336 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   50ef18d5 -- Merge branch '539-release-new-version' into 'master'
|\  
| * 7987f0e0 -- Resolve "Release new version"
|/  
* fcfa85cb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   d853702e -- Merge branch '538-latest-versions-update' into 'master'
|\  
| * 45b66042 -- Resolve "Latest versions update"
|/  
* 56660793 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   792cfe4f -- Merge branch '536-release-2021-12-0' into 'master'
|\  
| * b5948d67 -- Resolve "Release 2021.12.0"
|/  
*   470388a4 -- Merge branch '534-add-license-file' into 'master'
|\  
| * 0a8ddb6d -- Add license file
|/  
* 1e2e474b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   43180dec -- Merge branch '532-update-version-of-the-sandbox-agenda-and-training-agenda-due-to-guacamole-integration' into 'master'
|\  
| * c882bb6d -- Itegration of the Apache Guacamole - bump versions of the topology graph, training agenda and sandbox agenda.
|/  
* 076c437a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 14.0.0 Update to Angular 14. Add simulating tool.
*   094e8462 -- Merge branch 'develop' into 'master'
|\  
| * d640d8a9 -- Merge develop into master
|/  
* 2f6a791a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a8f74328 -- Merge branch 'develop' into 'master'
|\  
| * 5b90d9e7 -- Develop
|/  
* 3af8daf2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   36caaa2d -- Merge branch '551-bump-patch-version-of-training-agenda' into 'master'
|\  
| * a4b7a473 -- Resolve "Bump patch version of training agenda"
|/  
* 862c05e6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   da9a99f8 -- Merge branch '550-bump-versions-of-agendas' into 'master'
|\  
| * 94cb4df8 -- Resolve "Bump versions of agendas"
|/  
* 10664b3e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   335edd22 -- Merge branch 'develop' into 'master'
|\  
| * 117cdf64 -- Develop
|/  
* 66a67a89 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   bb2c4520 -- Merge branch '549-add-routing-for-run-visualization-tabs' into 'master'
|\  
| * c78c19a3 -- Resolve "Add routing for run visualization tabs"
|/  
* 410199da -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0d85b374 -- Merge branch 'develop' into 'master'
|\  
| * af863b0c -- Develop
|/  
* f092bd09 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0ae646c2 -- Merge branch 'develop' into 'master'
|\  
| * 0ff7f79e -- Create release candidate tag
|/  
* 31a8ec8f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a28b2dcb -- Merge branch 'fix-failing-pipeline' into 'master'
|\  
| * 743238e3 -- Fix failing pipeline
|/  
* 9f32a818 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   191c1c5a -- Merge branch '546-fix-failing-docker-push-pipeline-stage' into 'master'
|\  
| * 9ef9dcab -- Resolve "Fix failing docker push pipeline stage"
|/  
* b144800b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   6798b6a5 -- Merge branch '545-update-to-angular-13' into 'master'
|\  
| * 0452dbf7 -- Resolve "Update to Angular 13"
|/  
*   e6921b11 -- Merge branch '544-remove-tour-guide' into 'master'
|\  
| * 64eba553 -- Remove Tour guide
|/  
* 82d9c597 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   534273f0 -- Merge branch '543-bump-version' into 'master'
|\  
| * 66c4805a -- Bump version
|/  
* 1dc638f4 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   dcb7eea7 -- Merge branch '541-bump-version-of-sandbox-agenda' into 'master'
|\  
| * 577b9964 -- Resolve "Bump version of sandbox agenda"
|/  
* 9a5b7e4b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   95a7ea80 -- Merge branch '540-release-testing-fix-found-issues' into 'master'
|\  
| * 23d5291f -- Latest release
|/  
* 63200336 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   50ef18d5 -- Merge branch '539-release-new-version' into 'master'
|\  
| * 7987f0e0 -- Resolve "Release new version"
|/  
* fcfa85cb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   d853702e -- Merge branch '538-latest-versions-update' into 'master'
|\  
| * 45b66042 -- Resolve "Latest versions update"
|/  
* 56660793 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   792cfe4f -- Merge branch '536-release-2021-12-0' into 'master'
|\  
| * b5948d67 -- Resolve "Release 2021.12.0"
|/  
*   470388a4 -- Merge branch '534-add-license-file' into 'master'
|\  
| * 0a8ddb6d -- Add license file
|/  
* 1e2e474b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   43180dec -- Merge branch '532-update-version-of-the-sandbox-agenda-and-training-agenda-due-to-guacamole-integration' into 'master'
|\  
| * c882bb6d -- Itegration of the Apache Guacamole - bump versions of the topology graph, training agenda and sandbox agenda.
|/  
* 076c437a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 13.2.0 Summer release changes.
*   a8f74328 -- Merge branch 'develop' into 'master'
|\  
| * 5b90d9e7 -- Develop
|/  
* 3af8daf2 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   36caaa2d -- Merge branch '551-bump-patch-version-of-training-agenda' into 'master'
|\  
| * a4b7a473 -- Resolve "Bump patch version of training agenda"
|/  
* 862c05e6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   da9a99f8 -- Merge branch '550-bump-versions-of-agendas' into 'master'
|\  
| * 94cb4df8 -- Resolve "Bump versions of agendas"
|/  
* 10664b3e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   335edd22 -- Merge branch 'develop' into 'master'
|\  
| * 117cdf64 -- Develop
|/  
* 66a67a89 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   bb2c4520 -- Merge branch '549-add-routing-for-run-visualization-tabs' into 'master'
|\  
| * c78c19a3 -- Resolve "Add routing for run visualization tabs"
|/  
* 410199da -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0d85b374 -- Merge branch 'develop' into 'master'
|\  
| * af863b0c -- Develop
|/  
* f092bd09 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0ae646c2 -- Merge branch 'develop' into 'master'
|\  
| * 0ff7f79e -- Create release candidate tag
|/  
* 31a8ec8f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a28b2dcb -- Merge branch 'fix-failing-pipeline' into 'master'
|\  
| * 743238e3 -- Fix failing pipeline
|/  
* 9f32a818 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   191c1c5a -- Merge branch '546-fix-failing-docker-push-pipeline-stage' into 'master'
|\  
| * 9ef9dcab -- Resolve "Fix failing docker push pipeline stage"
|/  
* b144800b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   6798b6a5 -- Merge branch '545-update-to-angular-13' into 'master'
|\  
| * 0452dbf7 -- Resolve "Update to Angular 13"
|/  
*   e6921b11 -- Merge branch '544-remove-tour-guide' into 'master'
|\  
| * 64eba553 -- Remove Tour guide
|/  
* 82d9c597 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   534273f0 -- Merge branch '543-bump-version' into 'master'
|\  
| * 66c4805a -- Bump version
|/  
* 1dc638f4 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   dcb7eea7 -- Merge branch '541-bump-version-of-sandbox-agenda' into 'master'
|\  
| * 577b9964 -- Resolve "Bump version of sandbox agenda"
|/  
* 9a5b7e4b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   95a7ea80 -- Merge branch '540-release-testing-fix-found-issues' into 'master'
|\  
| * 23d5291f -- Latest release
|/  
* 63200336 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   50ef18d5 -- Merge branch '539-release-new-version' into 'master'
|\  
| * 7987f0e0 -- Resolve "Release new version"
|/  
* fcfa85cb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   d853702e -- Merge branch '538-latest-versions-update' into 'master'
|\  
| * 45b66042 -- Resolve "Latest versions update"
|/  
* 56660793 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   792cfe4f -- Merge branch '536-release-2021-12-0' into 'master'
|\  
| * b5948d67 -- Resolve "Release 2021.12.0"
|/  
*   470388a4 -- Merge branch '534-add-license-file' into 'master'
|\  
| * 0a8ddb6d -- Add license file
|/  
* 1e2e474b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   43180dec -- Merge branch '532-update-version-of-the-sandbox-agenda-and-training-agenda-due-to-guacamole-integration' into 'master'
|\  
| * c882bb6d -- Itegration of the Apache Guacamole - bump versions of the topology graph, training agenda and sandbox agenda.
|/  
* 076c437a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 13.1.1 Fix topology console open, optimize hurdling visualization polling strategy.
*   36caaa2d -- Merge branch '551-bump-patch-version-of-training-agenda' into 'master'
|\  
| * a4b7a473 -- Resolve "Bump patch version of training agenda"
|/  
* 862c05e6 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   da9a99f8 -- Merge branch '550-bump-versions-of-agendas' into 'master'
|\  
| * 94cb4df8 -- Resolve "Bump versions of agendas"
|/  
* 10664b3e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   335edd22 -- Merge branch 'develop' into 'master'
|\  
| * 117cdf64 -- Develop
|/  
* 66a67a89 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   bb2c4520 -- Merge branch '549-add-routing-for-run-visualization-tabs' into 'master'
|\  
| * c78c19a3 -- Resolve "Add routing for run visualization tabs"
|/  
* 410199da -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0d85b374 -- Merge branch 'develop' into 'master'
|\  
| * af863b0c -- Develop
|/  
* f092bd09 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0ae646c2 -- Merge branch 'develop' into 'master'
|\  
| * 0ff7f79e -- Create release candidate tag
|/  
* 31a8ec8f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a28b2dcb -- Merge branch 'fix-failing-pipeline' into 'master'
|\  
| * 743238e3 -- Fix failing pipeline
|/  
* 9f32a818 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   191c1c5a -- Merge branch '546-fix-failing-docker-push-pipeline-stage' into 'master'
|\  
| * 9ef9dcab -- Resolve "Fix failing docker push pipeline stage"
|/  
* b144800b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   6798b6a5 -- Merge branch '545-update-to-angular-13' into 'master'
|\  
| * 0452dbf7 -- Resolve "Update to Angular 13"
|/  
*   e6921b11 -- Merge branch '544-remove-tour-guide' into 'master'
|\  
| * 64eba553 -- Remove Tour guide
|/  
* 82d9c597 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   534273f0 -- Merge branch '543-bump-version' into 'master'
|\  
| * 66c4805a -- Bump version
|/  
* 1dc638f4 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   dcb7eea7 -- Merge branch '541-bump-version-of-sandbox-agenda' into 'master'
|\  
| * 577b9964 -- Resolve "Bump version of sandbox agenda"
|/  
* 9a5b7e4b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   95a7ea80 -- Merge branch '540-release-testing-fix-found-issues' into 'master'
|\  
| * 23d5291f -- Latest release
|/  
* 63200336 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   50ef18d5 -- Merge branch '539-release-new-version' into 'master'
|\  
| * 7987f0e0 -- Resolve "Release new version"
|/  
* fcfa85cb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   d853702e -- Merge branch '538-latest-versions-update' into 'master'
|\  
| * 45b66042 -- Resolve "Latest versions update"
|/  
* 56660793 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   792cfe4f -- Merge branch '536-release-2021-12-0' into 'master'
|\  
| * b5948d67 -- Resolve "Release 2021.12.0"
|/  
*   470388a4 -- Merge branch '534-add-license-file' into 'master'
|\  
| * 0a8ddb6d -- Add license file
|/  
* 1e2e474b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   43180dec -- Merge branch '532-update-version-of-the-sandbox-agenda-and-training-agenda-due-to-guacamole-integration' into 'master'
|\  
| * c882bb6d -- Itegration of the Apache Guacamole - bump versions of the topology graph, training agenda and sandbox agenda.
|/  
* 076c437a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 13.1.0 Add mitre techniques.
*   da9a99f8 -- Merge branch '550-bump-versions-of-agendas' into 'master'
|\  
| * 94cb4df8 -- Resolve "Bump versions of agendas"
|/  
* 10664b3e -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   335edd22 -- Merge branch 'develop' into 'master'
|\  
| * 117cdf64 -- Develop
|/  
* 66a67a89 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   bb2c4520 -- Merge branch '549-add-routing-for-run-visualization-tabs' into 'master'
|\  
| * c78c19a3 -- Resolve "Add routing for run visualization tabs"
|/  
* 410199da -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0d85b374 -- Merge branch 'develop' into 'master'
|\  
| * af863b0c -- Develop
|/  
* f092bd09 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0ae646c2 -- Merge branch 'develop' into 'master'
|\  
| * 0ff7f79e -- Create release candidate tag
|/  
* 31a8ec8f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a28b2dcb -- Merge branch 'fix-failing-pipeline' into 'master'
|\  
| * 743238e3 -- Fix failing pipeline
|/  
* 9f32a818 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   191c1c5a -- Merge branch '546-fix-failing-docker-push-pipeline-stage' into 'master'
|\  
| * 9ef9dcab -- Resolve "Fix failing docker push pipeline stage"
|/  
* b144800b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   6798b6a5 -- Merge branch '545-update-to-angular-13' into 'master'
|\  
| * 0452dbf7 -- Resolve "Update to Angular 13"
|/  
*   e6921b11 -- Merge branch '544-remove-tour-guide' into 'master'
|\  
| * 64eba553 -- Remove Tour guide
|/  
* 82d9c597 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   534273f0 -- Merge branch '543-bump-version' into 'master'
|\  
| * 66c4805a -- Bump version
|/  
* 1dc638f4 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   dcb7eea7 -- Merge branch '541-bump-version-of-sandbox-agenda' into 'master'
|\  
| * 577b9964 -- Resolve "Bump version of sandbox agenda"
|/  
* 9a5b7e4b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   95a7ea80 -- Merge branch '540-release-testing-fix-found-issues' into 'master'
|\  
| * 23d5291f -- Latest release
|/  
* 63200336 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   50ef18d5 -- Merge branch '539-release-new-version' into 'master'
|\  
| * 7987f0e0 -- Resolve "Release new version"
|/  
* fcfa85cb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   d853702e -- Merge branch '538-latest-versions-update' into 'master'
|\  
| * 45b66042 -- Resolve "Latest versions update"
|/  
* 56660793 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   792cfe4f -- Merge branch '536-release-2021-12-0' into 'master'
|\  
| * b5948d67 -- Resolve "Release 2021.12.0"
|/  
*   470388a4 -- Merge branch '534-add-license-file' into 'master'
|\  
| * 0a8ddb6d -- Add license file
|/  
* 1e2e474b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   43180dec -- Merge branch '532-update-version-of-the-sandbox-agenda-and-training-agenda-due-to-guacamole-integration' into 'master'
|\  
| * c882bb6d -- Itegration of the Apache Guacamole - bump versions of the topology graph, training agenda and sandbox agenda.
|/  
* 076c437a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 13.1.0-rc.2 Add routing for training run visualization tabs and fix minor issues with command visualizations.
*   335edd22 -- Merge branch 'develop' into 'master'
|\  
| * 117cdf64 -- Develop
|/  
* 66a67a89 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   bb2c4520 -- Merge branch '549-add-routing-for-run-visualization-tabs' into 'master'
|\  
| * c78c19a3 -- Resolve "Add routing for run visualization tabs"
|/  
* 410199da -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0d85b374 -- Merge branch 'develop' into 'master'
|\  
| * af863b0c -- Develop
|/  
* f092bd09 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0ae646c2 -- Merge branch 'develop' into 'master'
|\  
| * 0ff7f79e -- Create release candidate tag
|/  
* 31a8ec8f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a28b2dcb -- Merge branch 'fix-failing-pipeline' into 'master'
|\  
| * 743238e3 -- Fix failing pipeline
|/  
* 9f32a818 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   191c1c5a -- Merge branch '546-fix-failing-docker-push-pipeline-stage' into 'master'
|\  
| * 9ef9dcab -- Resolve "Fix failing docker push pipeline stage"
|/  
* b144800b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   6798b6a5 -- Merge branch '545-update-to-angular-13' into 'master'
|\  
| * 0452dbf7 -- Resolve "Update to Angular 13"
|/  
*   e6921b11 -- Merge branch '544-remove-tour-guide' into 'master'
|\  
| * 64eba553 -- Remove Tour guide
|/  
* 82d9c597 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   534273f0 -- Merge branch '543-bump-version' into 'master'
|\  
| * 66c4805a -- Bump version
|/  
* 1dc638f4 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   dcb7eea7 -- Merge branch '541-bump-version-of-sandbox-agenda' into 'master'
|\  
| * 577b9964 -- Resolve "Bump version of sandbox agenda"
|/  
* 9a5b7e4b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   95a7ea80 -- Merge branch '540-release-testing-fix-found-issues' into 'master'
|\  
| * 23d5291f -- Latest release
|/  
* 63200336 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   50ef18d5 -- Merge branch '539-release-new-version' into 'master'
|\  
| * 7987f0e0 -- Resolve "Release new version"
|/  
* fcfa85cb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   d853702e -- Merge branch '538-latest-versions-update' into 'master'
|\  
| * 45b66042 -- Resolve "Latest versions update"
|/  
* 56660793 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   792cfe4f -- Merge branch '536-release-2021-12-0' into 'master'
|\  
| * b5948d67 -- Resolve "Release 2021.12.0"
|/  
*   470388a4 -- Merge branch '534-add-license-file' into 'master'
|\  
| * 0a8ddb6d -- Add license file
|/  
* 1e2e474b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   43180dec -- Merge branch '532-update-version-of-the-sandbox-agenda-and-training-agenda-due-to-guacamole-integration' into 'master'
|\  
| * c882bb6d -- Itegration of the Apache Guacamole - bump versions of the topology graph, training agenda and sandbox agenda.
|/  
* 076c437a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 13.1.0-rc.2 Add routing for training run visualization tabs.
*   bb2c4520 -- Merge branch '549-add-routing-for-run-visualization-tabs' into 'master'
|\  
| * c78c19a3 -- Resolve "Add routing for run visualization tabs"
|/  
* 410199da -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0d85b374 -- Merge branch 'develop' into 'master'
|\  
| * af863b0c -- Develop
|/  
* f092bd09 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0ae646c2 -- Merge branch 'develop' into 'master'
|\  
| * 0ff7f79e -- Create release candidate tag
|/  
* 31a8ec8f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a28b2dcb -- Merge branch 'fix-failing-pipeline' into 'master'
|\  
| * 743238e3 -- Fix failing pipeline
|/  
* 9f32a818 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   191c1c5a -- Merge branch '546-fix-failing-docker-push-pipeline-stage' into 'master'
|\  
| * 9ef9dcab -- Resolve "Fix failing docker push pipeline stage"
|/  
* b144800b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   6798b6a5 -- Merge branch '545-update-to-angular-13' into 'master'
|\  
| * 0452dbf7 -- Resolve "Update to Angular 13"
|/  
*   e6921b11 -- Merge branch '544-remove-tour-guide' into 'master'
|\  
| * 64eba553 -- Remove Tour guide
|/  
* 82d9c597 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   534273f0 -- Merge branch '543-bump-version' into 'master'
|\  
| * 66c4805a -- Bump version
|/  
* 1dc638f4 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   dcb7eea7 -- Merge branch '541-bump-version-of-sandbox-agenda' into 'master'
|\  
| * 577b9964 -- Resolve "Bump version of sandbox agenda"
|/  
* 9a5b7e4b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   95a7ea80 -- Merge branch '540-release-testing-fix-found-issues' into 'master'
|\  
| * 23d5291f -- Latest release
|/  
* 63200336 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   50ef18d5 -- Merge branch '539-release-new-version' into 'master'
|\  
| * 7987f0e0 -- Resolve "Release new version"
|/  
* fcfa85cb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   d853702e -- Merge branch '538-latest-versions-update' into 'master'
|\  
| * 45b66042 -- Resolve "Latest versions update"
|/  
* 56660793 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   792cfe4f -- Merge branch '536-release-2021-12-0' into 'master'
|\  
| * b5948d67 -- Resolve "Release 2021.12.0"
|/  
*   470388a4 -- Merge branch '534-add-license-file' into 'master'
|\  
| * 0a8ddb6d -- Add license file
|/  
* 1e2e474b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   43180dec -- Merge branch '532-update-version-of-the-sandbox-agenda-and-training-agenda-due-to-guacamole-integration' into 'master'
|\  
| * c882bb6d -- Itegration of the Apache Guacamole - bump versions of the topology graph, training agenda and sandbox agenda.
|/  
* 076c437a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 13.1.0-rc.1 Update to dashboard, overview and hurdling visualizations to address the latest issues. Changes in linear and adaptive training definition removing minimal restrictions from solve time field and add trim to passkey and answer field. Fix pagination size in sandbox agenda.
*   0d85b374 -- Merge branch 'develop' into 'master'
|\  
| * af863b0c -- Develop
|/  
* f092bd09 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   0ae646c2 -- Merge branch 'develop' into 'master'
|\  
| * 0ff7f79e -- Create release candidate tag
|/  
* 31a8ec8f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a28b2dcb -- Merge branch 'fix-failing-pipeline' into 'master'
|\  
| * 743238e3 -- Fix failing pipeline
|/  
* 9f32a818 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   191c1c5a -- Merge branch '546-fix-failing-docker-push-pipeline-stage' into 'master'
|\  
| * 9ef9dcab -- Resolve "Fix failing docker push pipeline stage"
|/  
* b144800b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   6798b6a5 -- Merge branch '545-update-to-angular-13' into 'master'
|\  
| * 0452dbf7 -- Resolve "Update to Angular 13"
|/  
*   e6921b11 -- Merge branch '544-remove-tour-guide' into 'master'
|\  
| * 64eba553 -- Remove Tour guide
|/  
* 82d9c597 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   534273f0 -- Merge branch '543-bump-version' into 'master'
|\  
| * 66c4805a -- Bump version
|/  
* 1dc638f4 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   dcb7eea7 -- Merge branch '541-bump-version-of-sandbox-agenda' into 'master'
|\  
| * 577b9964 -- Resolve "Bump version of sandbox agenda"
|/  
* 9a5b7e4b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   95a7ea80 -- Merge branch '540-release-testing-fix-found-issues' into 'master'
|\  
| * 23d5291f -- Latest release
|/  
* 63200336 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   50ef18d5 -- Merge branch '539-release-new-version' into 'master'
|\  
| * 7987f0e0 -- Resolve "Release new version"
|/  
* fcfa85cb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   d853702e -- Merge branch '538-latest-versions-update' into 'master'
|\  
| * 45b66042 -- Resolve "Latest versions update"
|/  
* 56660793 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   792cfe4f -- Merge branch '536-release-2021-12-0' into 'master'
|\  
| * b5948d67 -- Resolve "Release 2021.12.0"
|/  
*   470388a4 -- Merge branch '534-add-license-file' into 'master'
|\  
| * 0a8ddb6d -- Add license file
|/  
* 1e2e474b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   43180dec -- Merge branch '532-update-version-of-the-sandbox-agenda-and-training-agenda-due-to-guacamole-integration' into 'master'
|\  
| * c882bb6d -- Itegration of the Apache Guacamole - bump versions of the topology graph, training agenda and sandbox agenda.
|/  
* 076c437a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 13.1.0-rc Preview of reference graph added, replaced openstack with terraform, topology can now be displayed for sandbox definition in overview table. Stage detail is now available from pipeline status bar.
*   0ae646c2 -- Merge branch 'develop' into 'master'
|\  
| * 0ff7f79e -- Create release candidate tag
|/  
* 31a8ec8f -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a28b2dcb -- Merge branch 'fix-failing-pipeline' into 'master'
|\  
| * 743238e3 -- Fix failing pipeline
|/  
* 9f32a818 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   191c1c5a -- Merge branch '546-fix-failing-docker-push-pipeline-stage' into 'master'
|\  
| * 9ef9dcab -- Resolve "Fix failing docker push pipeline stage"
|/  
* b144800b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   6798b6a5 -- Merge branch '545-update-to-angular-13' into 'master'
|\  
| * 0452dbf7 -- Resolve "Update to Angular 13"
|/  
*   e6921b11 -- Merge branch '544-remove-tour-guide' into 'master'
|\  
| * 64eba553 -- Remove Tour guide
|/  
* 82d9c597 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   534273f0 -- Merge branch '543-bump-version' into 'master'
|\  
| * 66c4805a -- Bump version
|/  
* 1dc638f4 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   dcb7eea7 -- Merge branch '541-bump-version-of-sandbox-agenda' into 'master'
|\  
| * 577b9964 -- Resolve "Bump version of sandbox agenda"
|/  
* 9a5b7e4b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   95a7ea80 -- Merge branch '540-release-testing-fix-found-issues' into 'master'
|\  
| * 23d5291f -- Latest release
|/  
* 63200336 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   50ef18d5 -- Merge branch '539-release-new-version' into 'master'
|\  
| * 7987f0e0 -- Resolve "Release new version"
|/  
* fcfa85cb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   d853702e -- Merge branch '538-latest-versions-update' into 'master'
|\  
| * 45b66042 -- Resolve "Latest versions update"
|/  
* 56660793 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   792cfe4f -- Merge branch '536-release-2021-12-0' into 'master'
|\  
| * b5948d67 -- Resolve "Release 2021.12.0"
|/  
*   470388a4 -- Merge branch '534-add-license-file' into 'master'
|\  
| * 0a8ddb6d -- Add license file
|/  
* 1e2e474b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   43180dec -- Merge branch '532-update-version-of-the-sandbox-agenda-and-training-agenda-due-to-guacamole-integration' into 'master'
|\  
| * c882bb6d -- Itegration of the Apache Guacamole - bump versions of the topology graph, training agenda and sandbox agenda.
|/  
* 076c437a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 13.0.1 Fix failing CI/CD.
*   a28b2dcb -- Merge branch 'fix-failing-pipeline' into 'master'
|\  
| * 743238e3 -- Fix failing pipeline
|/  
* 9f32a818 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   191c1c5a -- Merge branch '546-fix-failing-docker-push-pipeline-stage' into 'master'
|\  
| * 9ef9dcab -- Resolve "Fix failing docker push pipeline stage"
|/  
* b144800b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   6798b6a5 -- Merge branch '545-update-to-angular-13' into 'master'
|\  
| * 0452dbf7 -- Resolve "Update to Angular 13"
|/  
*   e6921b11 -- Merge branch '544-remove-tour-guide' into 'master'
|\  
| * 64eba553 -- Remove Tour guide
|/  
* 82d9c597 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   534273f0 -- Merge branch '543-bump-version' into 'master'
|\  
| * 66c4805a -- Bump version
|/  
* 1dc638f4 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   dcb7eea7 -- Merge branch '541-bump-version-of-sandbox-agenda' into 'master'
|\  
| * 577b9964 -- Resolve "Bump version of sandbox agenda"
|/  
* 9a5b7e4b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   95a7ea80 -- Merge branch '540-release-testing-fix-found-issues' into 'master'
|\  
| * 23d5291f -- Latest release
|/  
* 63200336 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   50ef18d5 -- Merge branch '539-release-new-version' into 'master'
|\  
| * 7987f0e0 -- Resolve "Release new version"
|/  
* fcfa85cb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   d853702e -- Merge branch '538-latest-versions-update' into 'master'
|\  
| * 45b66042 -- Resolve "Latest versions update"
|/  
* 56660793 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   792cfe4f -- Merge branch '536-release-2021-12-0' into 'master'
|\  
| * b5948d67 -- Resolve "Release 2021.12.0"
|/  
*   470388a4 -- Merge branch '534-add-license-file' into 'master'
|\  
| * 0a8ddb6d -- Add license file
|/  
* 1e2e474b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   43180dec -- Merge branch '532-update-version-of-the-sandbox-agenda-and-training-agenda-due-to-guacamole-integration' into 'master'
|\  
| * c882bb6d -- Itegration of the Apache Guacamole - bump versions of the topology graph, training agenda and sandbox agenda.
|/  
* 076c437a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 13.0.0 Update to Angular 13, CI/CD optimization, support for usage of local sandboxes, allocation pipeline retry, preview of adaptive definition added, bug fixes.
*   191c1c5a -- Merge branch '546-fix-failing-docker-push-pipeline-stage' into 'master'
|\  
| * 9ef9dcab -- Resolve "Fix failing docker push pipeline stage"
|/  
* b144800b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   6798b6a5 -- Merge branch '545-update-to-angular-13' into 'master'
|\  
| * 0452dbf7 -- Resolve "Update to Angular 13"
|/  
*   e6921b11 -- Merge branch '544-remove-tour-guide' into 'master'
|\  
| * 64eba553 -- Remove Tour guide
|/  
* 82d9c597 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   534273f0 -- Merge branch '543-bump-version' into 'master'
|\  
| * 66c4805a -- Bump version
|/  
* 1dc638f4 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   dcb7eea7 -- Merge branch '541-bump-version-of-sandbox-agenda' into 'master'
|\  
| * 577b9964 -- Resolve "Bump version of sandbox agenda"
|/  
* 9a5b7e4b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   95a7ea80 -- Merge branch '540-release-testing-fix-found-issues' into 'master'
|\  
| * 23d5291f -- Latest release
|/  
* 63200336 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   50ef18d5 -- Merge branch '539-release-new-version' into 'master'
|\  
| * 7987f0e0 -- Resolve "Release new version"
|/  
* fcfa85cb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   d853702e -- Merge branch '538-latest-versions-update' into 'master'
|\  
| * 45b66042 -- Resolve "Latest versions update"
|/  
* 56660793 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   792cfe4f -- Merge branch '536-release-2021-12-0' into 'master'
|\  
| * b5948d67 -- Resolve "Release 2021.12.0"
|/  
*   470388a4 -- Merge branch '534-add-license-file' into 'master'
|\  
| * 0a8ddb6d -- Add license file
|/  
* 1e2e474b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   43180dec -- Merge branch '532-update-version-of-the-sandbox-agenda-and-training-agenda-due-to-guacamole-integration' into 'master'
|\  
| * c882bb6d -- Itegration of the Apache Guacamole - bump versions of the topology graph, training agenda and sandbox agenda.
|/  
* 076c437a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 13.0.0 Update to Angular 13, CI/CD optimization, support for local sandboxes, allocation pipeline retry, preview of adaptive definition added, bug fixes.
*   6798b6a5 -- Merge branch '545-update-to-angular-13' into 'master'
|\  
| * 0452dbf7 -- Resolve "Update to Angular 13"
|/  
*   e6921b11 -- Merge branch '544-remove-tour-guide' into 'master'
|\  
| * 64eba553 -- Remove Tour guide
|/  
* 82d9c597 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   534273f0 -- Merge branch '543-bump-version' into 'master'
|\  
| * 66c4805a -- Bump version
|/  
* 1dc638f4 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   dcb7eea7 -- Merge branch '541-bump-version-of-sandbox-agenda' into 'master'
|\  
| * 577b9964 -- Resolve "Bump version of sandbox agenda"
|/  
* 9a5b7e4b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   95a7ea80 -- Merge branch '540-release-testing-fix-found-issues' into 'master'
|\  
| * 23d5291f -- Latest release
|/  
* 63200336 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   50ef18d5 -- Merge branch '539-release-new-version' into 'master'
|\  
| * 7987f0e0 -- Resolve "Release new version"
|/  
* fcfa85cb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   d853702e -- Merge branch '538-latest-versions-update' into 'master'
|\  
| * 45b66042 -- Resolve "Latest versions update"
|/  
* 56660793 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   792cfe4f -- Merge branch '536-release-2021-12-0' into 'master'
|\  
| * b5948d67 -- Resolve "Release 2021.12.0"
|/  
*   470388a4 -- Merge branch '534-add-license-file' into 'master'
|\  
| * 0a8ddb6d -- Add license file
|/  
* 1e2e474b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   43180dec -- Merge branch '532-update-version-of-the-sandbox-agenda-and-training-agenda-due-to-guacamole-integration' into 'master'
|\  
| * c882bb6d -- Itegration of the Apache Guacamole - bump versions of the topology graph, training agenda and sandbox agenda.
|/  
* 076c437a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 12.1.5 Bump version of training agenda.
*   534273f0 -- Merge branch '543-bump-version' into 'master'
|\  
| * 66c4805a -- Bump version
|/  
* 1dc638f4 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   dcb7eea7 -- Merge branch '541-bump-version-of-sandbox-agenda' into 'master'
|\  
| * 577b9964 -- Resolve "Bump version of sandbox agenda"
|/  
* 9a5b7e4b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   95a7ea80 -- Merge branch '540-release-testing-fix-found-issues' into 'master'
|\  
| * 23d5291f -- Latest release
|/  
* 63200336 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   50ef18d5 -- Merge branch '539-release-new-version' into 'master'
|\  
| * 7987f0e0 -- Resolve "Release new version"
|/  
* fcfa85cb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   d853702e -- Merge branch '538-latest-versions-update' into 'master'
|\  
| * 45b66042 -- Resolve "Latest versions update"
|/  
* 56660793 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   792cfe4f -- Merge branch '536-release-2021-12-0' into 'master'
|\  
| * b5948d67 -- Resolve "Release 2021.12.0"
|/  
*   470388a4 -- Merge branch '534-add-license-file' into 'master'
|\  
| * 0a8ddb6d -- Add license file
|/  
* 1e2e474b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   43180dec -- Merge branch '532-update-version-of-the-sandbox-agenda-and-training-agenda-due-to-guacamole-integration' into 'master'
|\  
| * c882bb6d -- Itegration of the Apache Guacamole - bump versions of the topology graph, training agenda and sandbox agenda.
|/  
* 076c437a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 12.1.4 Bump version of sandbox agenda.
*   dcb7eea7 -- Merge branch '541-bump-version-of-sandbox-agenda' into 'master'
|\  
| * 577b9964 -- Resolve "Bump version of sandbox agenda"
|/  
* 9a5b7e4b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   95a7ea80 -- Merge branch '540-release-testing-fix-found-issues' into 'master'
|\  
| * 23d5291f -- Latest release
|/  
* 63200336 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   50ef18d5 -- Merge branch '539-release-new-version' into 'master'
|\  
| * 7987f0e0 -- Resolve "Release new version"
|/  
* fcfa85cb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   d853702e -- Merge branch '538-latest-versions-update' into 'master'
|\  
| * 45b66042 -- Resolve "Latest versions update"
|/  
* 56660793 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   792cfe4f -- Merge branch '536-release-2021-12-0' into 'master'
|\  
| * b5948d67 -- Resolve "Release 2021.12.0"
|/  
*   470388a4 -- Merge branch '534-add-license-file' into 'master'
|\  
| * 0a8ddb6d -- Add license file
|/  
* 1e2e474b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   43180dec -- Merge branch '532-update-version-of-the-sandbox-agenda-and-training-agenda-due-to-guacamole-integration' into 'master'
|\  
| * c882bb6d -- Itegration of the Apache Guacamole - bump versions of the topology graph, training agenda and sandbox agenda.
|/  
* 076c437a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 12.1.3 Bump version of training agenda, sandbox agenda and api.
*   95a7ea80 -- Merge branch '540-release-testing-fix-found-issues' into 'master'
|\  
| * 23d5291f -- Latest release
|/  
* 63200336 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   50ef18d5 -- Merge branch '539-release-new-version' into 'master'
|\  
| * 7987f0e0 -- Resolve "Release new version"
|/  
* fcfa85cb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   d853702e -- Merge branch '538-latest-versions-update' into 'master'
|\  
| * 45b66042 -- Resolve "Latest versions update"
|/  
* 56660793 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   792cfe4f -- Merge branch '536-release-2021-12-0' into 'master'
|\  
| * b5948d67 -- Resolve "Release 2021.12.0"
|/  
*   470388a4 -- Merge branch '534-add-license-file' into 'master'
|\  
| * 0a8ddb6d -- Add license file
|/  
* 1e2e474b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   43180dec -- Merge branch '532-update-version-of-the-sandbox-agenda-and-training-agenda-due-to-guacamole-integration' into 'master'
|\  
| * c882bb6d -- Itegration of the Apache Guacamole - bump versions of the topology graph, training agenda and sandbox agenda.
|/  
* 076c437a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 12.1.2 More bug fixes
*   50ef18d5 -- Merge branch '539-release-new-version' into 'master'
|\  
| * 7987f0e0 -- Resolve "Release new version"
|/  
* fcfa85cb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   d853702e -- Merge branch '538-latest-versions-update' into 'master'
|\  
| * 45b66042 -- Resolve "Latest versions update"
|/  
* 56660793 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   792cfe4f -- Merge branch '536-release-2021-12-0' into 'master'
|\  
| * b5948d67 -- Resolve "Release 2021.12.0"
|/  
*   470388a4 -- Merge branch '534-add-license-file' into 'master'
|\  
| * 0a8ddb6d -- Add license file
|/  
* 1e2e474b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   43180dec -- Merge branch '532-update-version-of-the-sandbox-agenda-and-training-agenda-due-to-guacamole-integration' into 'master'
|\  
| * c882bb6d -- Itegration of the Apache Guacamole - bump versions of the topology graph, training agenda and sandbox agenda.
|/  
* 076c437a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 12.1.1 Bug fixes
*   d853702e -- Merge branch '538-latest-versions-update' into 'master'
|\  
| * 45b66042 -- Resolve "Latest versions update"
|/  
* 56660793 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   792cfe4f -- Merge branch '536-release-2021-12-0' into 'master'
|\  
| * b5948d67 -- Resolve "Release 2021.12.0"
|/  
*   470388a4 -- Merge branch '534-add-license-file' into 'master'
|\  
| * 0a8ddb6d -- Add license file
|/  
* 1e2e474b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   43180dec -- Merge branch '532-update-version-of-the-sandbox-agenda-and-training-agenda-due-to-guacamole-integration' into 'master'
|\  
| * c882bb6d -- Itegration of the Apache Guacamole - bump versions of the topology graph, training agenda and sandbox agenda.
|/  
* 076c437a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 12.1.0 Add adaptive transition visualization, support of APG, dashboard and command analysis for linear training definitions, detail pages for agendas, topology refactor.
*   792cfe4f -- Merge branch '536-release-2021-12-0' into 'master'
|\  
| * b5948d67 -- Resolve "Release 2021.12.0"
|/  
*   470388a4 -- Merge branch '534-add-license-file' into 'master'
|\  
| * 0a8ddb6d -- Add license file
|/  
* 1e2e474b -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   43180dec -- Merge branch '532-update-version-of-the-sandbox-agenda-and-training-agenda-due-to-guacamole-integration' into 'master'
|\  
| * c882bb6d -- Itegration of the Apache Guacamole - bump versions of the topology graph, training agenda and sandbox agenda.
|/  
* 076c437a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 12.0.9 Integration of the Apache Guacamole to topology graph
*   43180dec -- Merge branch '532-update-version-of-the-sandbox-agenda-and-training-agenda-due-to-guacamole-integration' into 'master'
|\  
| * c882bb6d -- Itegration of the Apache Guacamole - bump versions of the topology graph, training agenda and sandbox agenda.
|/  
* 076c437a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 12.0.8 Summer clean up hotfix
*   f393f01a -- Merge branch '531-create-latest-tag' into 'master'
|\  
| * c9036521 -- Tag message
|/  
* f14ffdeb -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 12.0.7 Bump version of training agenda and overview visualization
*   711c3507 -- Merge branch '530-create-tag-with-latest-changes' into 'master'
|\  
| * bbb9952b -- Bump training agenda and overview viz, tag message
|/  
* d9703bc1 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 12.0.7 Add version to kypo config
*   e0855d30 -- Merge branch '529-add-version-to-kypo-config' into 'master'
|\  
| * a668ac7c -- Resolve "Add version to kypo config"
|/  
* 2ed5b4de -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 12.0.6 Bump version of training agenda
*   edda297a -- Merge branch '528-fix-version' into 'master'
|\  
| * e964fe90 -- Fix version
|/  
* 294e72dc -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 12.0.5 Bump version of training agenda
*   94d071bd -- Merge branch '527-bump-version' into 'master'
|\  
| * dd5cf599 -- Bump version of training agenda
|/  
* b0a3c153 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 12.0.5 Add theme for sentinel markdown
*   779dfa38 -- Merge branch '526-add-theme-for-sentinel-markdown' into 'master'
|\  
| * 514a0a79 -- Resolve "Add theme for sentinel markdown"
|/  
* 62aa2419 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 12.0.4 Summer clean up
*   a69640a8 -- Merge branch '525-create-tag-with-latest-changes' into 'master'
|\  
| * a2fea0ef -- Versions update, tag message
|/  
*   4ef23e51 -- Merge branch '524-rename-game-level-to-training-level' into 'master'
|\  
| * 506b99e0 -- Resolve "Rename game level to training level"
|/  
*   c74681f3 -- Merge branch '523-update-sentinel-common-to-reduce-bundle-size' into 'master'
|\  
| * f48946cd -- Bump version of sentinel
|/  
*   22e250e5 -- Merge branch '522-implement-preloading-strategy-based-on-user-roles' into 'master'
|\  
| * 632e2b60 -- Resolve "Implement preloading strategy based on user roles"
|/  
*   5289f4a2 -- Merge branch '521-fix-local-config-paths' into 'master'
|\  
| * a0eca76a -- Fix paths
|/  
*   1fea2ecc -- Merge branch '520-format-content-in-markdown-shown-in-kypo-portal-as-it-is-shown-by-gitlab' into 'master'
|\  
| * 50f7e3a4 -- Add background to markdown code
|/  
* dc6ca32a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 12.0.3 Bump version of training agenda
*   ea024c9b -- Merge branch '519-bump-version-of-training-agenda' into 'master'
|\  
| * 7ed76a41 -- Resolve "Bump version of training agenda"
|/  
* 6c421c72 -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
### 12.0.2 Update giltab CI
*   953fb6e8 -- Merge branch '518-simplify-gitlab-ci-cd-using-csirt-mu-docker-image' into 'master'
|\  
| * 0767e1b4 -- Resolve "Simplify Gitlab CI/CD using CSIRT-MU Docker image"
|/  
*   d5454645 -- Merge branch '517-fix-docker-file' into 'master'
|\  
| * 8f7baa9e -- Fix dockerFile
|/  
*   1fccaf1c -- Merge branch '514-update-to-angular-12' into 'master'
|\  
| * fa794ca7 -- Resolve "Update to Angular 12"
|/  
*   f773747c -- Merge branch '515-add-version-of-application-to-navigation' into 'master'
|\  
| * ee21bc50 -- Resolve "Add version of application to navigation"
|/  
*   ed9ff0ca -- Merge branch '516-bump-training-agenda-version' into 'master'
|\  
| * 7cf65df6 -- training agenda bumped
|/  
*   6a808641 -- Merge branch '513-bump-version-of-training-agenda' into 'master'
|\  
| * 27a357e4 -- Resolve "Bump version of training agenda"
|/  
*   ee9e748e -- Merge branch '512-bump-version-of-adaptive-trainings-visualization' into 'master'
|\  
| * 85812b5a -- Bump version of adaptive trainings visualization
|/  
*   b2197caa -- Merge branch '511-bump-version-of-training-agenda' into 'master'
|\  
| * 408c6b2b -- Bump agenda, api and model version
|/  
*   9727a472 -- Merge branch '510-add-adaptive-training-url-to-error-handler' into 'master'
|\  
| * c4a81d8d -- adaptive url added to error handler
|/  
*   c1513abc -- Merge branch '477-integrate-new-progress-visualizations' into 'master'
|\  
| * 94d4ea9f -- Bump versions of training agenda and hurdling
|/  
*   4412c954 -- Merge branch '509-bumb-training-api-and-training-agenda' into 'master'
|\  
| * 70b17334 -- training agenda and api bumped
|/  
*   346aac75 -- Merge branch '508-bump-version-of-training-agenda' into 'master'
|\  
| * fc55ed11 -- Bump version of training agenda
|/  
*   4a069186 -- Merge branch '507-update-training-agenda-version' into 'master'
|\  
| * e78d303f -- Agenda version update
|/  
*   83bd96fd -- Merge branch '506-update-training-agenda-version' into 'master'
|\  
| * df32a0c7 -- Training agenda version update
|/  
*   7764d6d6 -- Merge branch '505-remove-unused-import-causing-errror' into 'master'
|\  
| * 21ab3eb0 -- Resolve "Remove unused import causing errror"
|/  
*   7377699f -- Merge branch '504-integrate-adaptive-learning-techniques' into 'master'
|\  
| * 0b521367 -- Resolve "Integrate Adaptive learning techniques"
|/  
*   5483267c -- Merge branch '502-update-datetime-picker' into 'master'
|\  
| * a2e7bd7b -- Update date-time-picker to latest version that supports Angular 11
* |   1734475f -- Merge branch '503-update-oidc-configuration-and-readme' into 'master'
|\ \  
| |/  
|/|   
| * 44b44da5 -- Readme and oidc update
|/  
*   c41ccb2b -- Merge branch '501-update-to-angular-11' into 'master'
|\  
| * e5bf1174 -- Resolve "Update to Angular 11"
|/  
*   73270ef2 -- Merge branch '500-migrate-from-tslint-to-eslint' into 'master'
|\  
| * 9187cc26 -- Migrate to eslint, fix errors/warnings
|/  
*   35693e9a -- Merge branch '499-change-docker-image-push-name' into 'master'
|\  
| * 369e0e80 -- change image name
|/  
*   64a8047d -- Merge branch '498-edit-docker-puch-pipeline-to-push-to-gitlab-registry' into 'master'
|\  
| * 722a3047 -- Change docker image push stage to push to gitlab registry
|/  
*   d85e19b5 -- Merge branch '497-setup-ci-to-push-docker-containers-on-new-tag' into 'master'
|\  
| * c12f1509 -- Resolve "Setup CI to push docker containers on new tag"
|/  
*   8b0c359b -- Merge branch '496-add-support-for-scoped-registry-in-dockerfile' into 'master'
|\  
| * 1ec03654 -- edit dockerfile to support scoped registry
|/  
*   43c6cb6e -- Merge branch '495-recreate-package-lock-for-new-package-registry' into 'master'
|\  
| * 5827f0bb -- recreate package lock
|/  
*   821bbdc0 -- Merge branch '494-rename-package-scope-to-muni-kypo-crp' into 'master'
|\  
| * 466a1a36 -- Update dependencies
|/  
*   86e830d9 -- Merge branch '493-update-dependencies-to-new-format' into 'master'
|\  
| * aba0f753 -- Update dependencies
|/  
*   1221c2c8 -- Merge branch '492-migrate-to-sentinel-auth' into 'master'
|\  
| * 0812edb6 -- Remove dependency on kypo2-auth
|/  
*   4d73f2b5 -- Merge branch 'patch-1' into 'master'
|\  
| * 654c945f -- Update Dockerfile
|/  
*   75dedbc6 -- Merge branch '490-rename-organizer-and-designer-to-instructor-in-portal-page' into 'master'
|\  
| * 117221ae -- Updated sentinel and ngx-cookie version, renamed organizer to instructor
|/  
*   2f85dcb6 -- Merge branch '489-update-to-sentinel-common-and-refactor-dynamic-environments' into 'master'
|\  
| * 8f905bc9 -- Update @sentinel/common and replace the custom solution of dynamic environments
|/  
*   d7d32e9d -- Merge branch '488-integrate-new-version-of-overview-visualizations' into 'master'
|\  
| * 2f298dd4 -- Resolve "Integrate new version of overview visualizations"
|/  
*   a94855dd -- Merge branch '475-integrate-tour-guide-component' into 'master'
|\  
| * 916e4f80 -- Added newer version of sentinel to dependencies, added tour guide styles, added config file for tours
|/  
*   9712458f -- Merge branch '487-clear-personal-data-of-developers-from-the-source-code' into 'master'
|\  
| * 46d64ffa -- Resolve "Clear personal data of developers from the source code"
|/  
*   b2093ca9 -- Merge branch '476-implement-new-main-page-of-kypo-portal' into 'master'
|\  
| * d501194b -- Reworked home page, added button elevation on text hover and text elevation on button hover. Based on the user role permitted actions and descriptions are displayed. Created new models to simplify agenda creation. Updated resposivity to match new layout
|/  
*   57233882 -- Merge branch '486-update-sentinel-components' into 'master'
|\  
| * 98a887fd -- Update sentinel components
|/  
*   7fd0d5bc -- Merge branch '485-add-consent-text' into 'master'
|\  
| * c768973f -- Update consent text
|/  
*   e532dc29 -- Merge branch '484-replace-kypo2-auth-with-sentinel-auth' into 'master'
|\  
| * d54aeb79 -- Resolve "Replace kypo2-auth with @sentinel/auth"
|/  
*   53d391c3 -- Merge branch '483-integrate-with-new-version-of-sandbox-agenda' into 'master'
|\  
| * 6b8e7e59 -- Resolve "Integrate with new version of sandbox agenda"
|/  
*   81639abf -- Merge branch '482-add-missing-import-to-training-run-detail-module' into 'master'
|\  
| * 8a26cc38 -- Added missing import to module
|/  
*   fb96f354 -- Merge branch '481-integrate-with-new-version-of-uag-agenda' into 'master'
|\  
| * 9749c365 -- Resolve "Integrate with new version of uag agenda"
* |   ee352a08 -- Merge branch '480-integrate-new-versions-of-agendas' into 'master'
|\ \  
| |/  
|/|   
| * d7199070 -- Resolve "Integrate new versions of agendas"
|/  
*   a343c700 -- Merge branch '479-update-auth-to-latest-release' into 'master'
|\  
| * 20e5317b -- Updated kypo-auth to latest release
|/  
*   52e7324e -- Merge branch '478-update-dependencies-to-reflect-changes-in-training-microservice-api' into 'master'
|\  
| * b069cd91 -- Update dependencies
|/  
*   2f7ca974 -- Merge branch '474-change-baseurl-for-visualization-components' into 'master'
|\  
| * 30d5de95 -- Resolve "Change baseUrl for visualization components"
|/  
*   b9a5efb2 -- Merge branch '473-run-the-ci-in-node-docker-image' into 'master'
|\  
| * 3bdb75ae -- Resolve "Run the CI in node docker image"
|/  
*   0ea831be -- Merge branch '472-optimize-imports-of-agendas' into 'master'
|\  
| * 4c05e2b3 -- Optimize imports of agendas by usage of secondary entry points
|/  
*   2a05db7a -- Merge branch '471-update-dependencies-to-remove-warnings-during-build' into 'master'
|\  
| * 0aafe78d -- Update packages to remove internal imports warnings
|/  
*   41bf90c3 -- Merge branch '470-replace-csirt-and-kypo-libraries-with-sentinel' into 'master'
|\  
| * f2c1e4f8 -- Replace csirt and kypo libraries with sentinel
|/  
*   c93b12eb -- Merge branch '469-update-agendas-and-sandbox-api-model-dependencies' into 'master'
|\  
| * 6ceb0fe4 -- Resolve "Update agendas and sandbox api/model dependencies"
|/  
*   553b0731 -- Merge branch '468-update-to-angular-10' into 'master'
|\  
| * 1e54ef19 -- Resolve "Update to Angular 10"
|/  
* 67d29bd6 -- Merge branch 'kypo-demo-hotfix' into 'master'
* 2bf620d1 -- Update sandbox agenda version
